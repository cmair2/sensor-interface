#!/bin/sh

GIT_USERNAME=cmair2
OLT_SERVER_IP=192.168.2.100


# Do not change stuff below this line. You have been warned!

# OLT has to be right next to our scripts for everything to work. Make sure we are not in ~/<dir>, but in ~/<dir>/<dir>!
OLT_DIR=../OLT

USER=`whoami`
CURRENT_DIR=`pwd`
echo "INFO: using ${USERNAME} as username for lightelligence git operations."
echo "INFO: all scripts will be installed to run as user ${USER}."
echo "----------------------------------------------------------------------"

echo "Setup BASEDIR for sensor scripts..."
sed -i "/^BASEDIR=/c\BASEDIR=${CURRENT_DIR}" start-sensors.sh

echo "Creating directory ${OLT_DIR}"
mkdir ${OLT_DIR}
cd ${OLT_DIR}

echo "Cloning git repositories..."
echo
git clone https://${GIT_USERNAME}@bitbucket.org/lightelligence/device.git
git clone https://${GIT_USERNAME}@bitbucket.org/lightelligence/device-sdk.git

if [ "x$1" = "xoffline" ]; then
	echo
	echo "Fixing /etc/hosts to point domains to ${OLT_SERVER_IP}"
	# We redirect both domains as the device-sdk connects to api.int.oltd.de as default and fixing that is more complicated than this.
        # Also this avoids certificate issues due to wrong host names.
	sudo sed -i "\$a${OLT_SERVER_IP} api.int.oltd.de" /etc/hosts
	sudo sed -i "\$a${OLT_SERVER_IP} api.lightelligence.io" /etc/hosts
fi

echo
echo "Creating entry in /etc/rc.local to autostart everything"
sudo sed -i "\$isu $USER -l -c \"/bin/sh /home/${USER}/DS-Sensors/sensorboard-viewer/start-sensors.sh\"" /etc/rc.local

echo
echo "Starting OLT setup... If this does not work, check if the server is reachabe via ${OLT_SERVER_IP}"
cd device-sdk/examples/irlynx-presence
python3 setup-olt.py

echo
echo "We are done now! Reboot and hope for the best!"
