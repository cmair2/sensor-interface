import os
import signal
import subprocess
import sys
from threading import Timer
from generic_sensor import GenericSensor

class Watchdog(GenericSensor):

    sensor_type = 'watchdog'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self.load_config()
        self._pids = {}
        self._check_timer = Timer(2, self.timer_timeout)

    def load_config(self):
        pass

    def start(self):
        self.mqtt_start()
        self._check_timer.start()

    def stop(self):
        self.mqtt_stop()

    def updatePIDs(self):
        for script in dict(self._pids):
            process = self._pids[script]
            if process.poll() is not None:
                print('Process {} terminated with return code {}'.format(process.args, process.returncode))
                del self._pids[script]

    def start_missing_process(self):
        self.updatePIDs()
        for section in self.config.sections():
            if section not in self._pids.keys() and section != self.name:
                script = self.config[section].get('script')
                auto = self.config[section].get('autostart')
                if script and auto:
                    print(auto.value)
                    if auto.value == "true":
                        script = script.value.strip()
                        print("Starting {} ({})".format(section, script))
                        cmd = ["/usr/bin/screen", "-D", "-m", "-S", "{}".format(section), "/usr/bin/python3", "{}/{}".format(os.path.dirname(os.path.realpath(sys.argv[0])), script), "{}".format(section)]
                        p = subprocess.Popen(cmd)
                        self._pids[section] = p

    def timer_timeout(self):
        self.start_missing_process()
        self._check_timer = Timer(2, self.timer_timeout)
        self._check_timer.start()


if __name__ == '__main__':
    watchdog = Watchdog('watchdog')

    def signal_handler(signal_frame):
        def run(*args):
            print('Stopping watchdog')
            watchdog.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    watchdog.start()
