import json
import signal
import selectors
import serial
import socket
import struct
import sys
import threading
import numpy as np
from threading import Thread, Timer
from generic_sensor import GenericSensor


class GrideyePeoplecounter(GenericSensor):

    raw_image_invert = False
    raw_image_rescale = True
    coordinates_normalize = 3840
    sensor_type = 'peoplecounter_absolute'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self.load_config()
        self.persons = {}
        self.dimensions = []
        self.raw_image = b''
        self._stop_reading_sensor = False
        self._stop_report_timer = False
        self._stop_listening = False
        self.report_timer = None
        self.selectors = selectors.DefaultSelector()
        self.clients = []
        self.raw_data = []

    def load_config(self):
        self.serialport = self.load_config_value(self.name, 'serialport', default='/dev/ttyUSB0')
        self.baudrate = self.load_config_value(self.name, 'baudrate', default='19200')
        self.debughost = self.load_config_value(self.name, 'debughost', default='0.0.0.0')
        self.debugport = int(self.load_config_value(self.name, 'debugport', default='42002'))

    def publish_sensor_data(self):
        self.report_timer_restart(5, self.publish_sensor_data)
        msg = {
            "count": len(self.persons),
            "coords": self.persons,
            "rawData": self.raw_data,
        }
#        print(msg)
        self.publish(msg)

    def report_timer_restart(self, timeout, method):
        self.report_timer.cancel()
        if not self._stop_report_timer:
            self.report_timer = Timer(timeout, method)
            self.report_timer.start()

    def start_timers(self, ser):
        self.report_timer = Timer(5, self.publish_sensor_data)
        self.report_timer.start()

    def start(self):
        self.mqtt_start()
        self.read_sensor()

    def stop(self):
        self._stop_reading_sensor = True
        self._stop_listening = True
        self._stop_report_timer = True
        if self.report_timer:
            self.report_timer.cancel()
        self.mqtt_stop()

    def accept(self, sock, mask):
        conn, addr = sock.accept()
        print('Connection {} from {} accepted'.format(conn, addr))
        self.selectors.register(conn, selectors.EVENT_READ, self.read)
        self.clients.append(conn)
        print('Clients: {}'.format(self.clients))

    def read(self, conn, mask):
        try:
            data = conn.recv(1000)
            if data:
                print('got {} from {}'.format(repr(data), conn))
        except Exception as e:
            print('Closing {}'.format(conn))
            try:
                self.selectors.unregister(conn)
            except:
                print('Can\'t unregister connection {}'.format(conn))
            try:
                conn.close()
            except:
                print('Can\'t close connection {}'.format(conn))
            try:
                self.clients.remove(conn)
            except:
                print('Can\'t remove conn {}'.format(conn))

    def debug_listener(self):
        self.sock = socket.socket()
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.debughost, self.debugport))
        self.sock.listen(100)
        self.sock.setblocking(False)
        self.selectors.register(self.sock, selectors.EVENT_READ, self.accept)

        print("Listening...")
        try:
            while not self._stop_listening:
                events = self.selectors.select(1)
                for key, mask in events:
                    callback = key.data
                    callback(key.fileobj, mask)

        except Exception as e:
            print("Terminate: " + str(e))
            self.sock.close()
            for conn in self.clients:
                conn.close()

    def send_debug(self, payload):
        for conn in list(self.clients):
            try:
                conn.send(payload)
            except:
                try:
                    self.selectors.unregister(conn)
                except:
                    print('Can\'t unregister connection {}'.format(conn))
                try:
                    conn.close()
                except:
                    print('Can\'t close connection {}'.format(conn))
                try:
                    self.clients.remove(conn)
                except:
                    print('Can\'t remove conn {}'.format(conn))

    def read_sensor(self):
        tcpserver = threading.Thread(target=self.debug_listener)
        tcpserver.start()

        synchronized = False
        try:
            with serial.Serial(self.serialport, self.baudrate, timeout=1) as ser:
                self.start_timers(ser)
                while not self._stop_reading_sensor:

                    if not synchronized:
                        byte = ser.read(1)
                        if byte == b'*':
                            byte = ser.read(2)
                            if byte == b'**':
                                synchronized = True
#                                print("sync'd")
                        else:
                            print("No SYNC: {}".format(byte))
                            self.send_debug(byte)
                    else:
                        byte = ser.read(2+(64*2)+2) # read possible EOL marker
#                        print(binascii.hexlify(byte))
                        raw = struct.unpack('<64H', byte[2:130])
#                        raw = [(-q12) if (q12 & 0x0800 == 0x0800) else q12 for q12 in raw]
                        raw = [(q12 | 0xf800) if (q12 & 0x0800) else q12 for q12 in raw]
                        temp = np.reshape(np.array(raw)*64/256, (8,8))
                        self.raw_data = temp.tolist()
#                        print(temp)
                        if not ((byte[-2] == 13) and (byte[-1] == 10)):
                            # We did not read a EOL marker, read more data as we are dealing with TrackMe firmware
#                            print("TrackMe extended data set detected!")
                            byte = byte + ser.read(225+1+2+2 -2)    # not existent EOL marker was part of the first 255 -> read two bytes less
                            pcount = int(byte[-5])
                            person_info = ser.read(pcount*9)
                            byte = byte + person_info
                            if pcount > 0:
                                self.persons = {x[0]: {'x': x[4]/self.coordinates_normalize, 'y': x[3]/self.coordinates_normalize } for x in struct.iter_unpack('>BHHHH', person_info)}
                            else:
                                self.persons = {}
#                            if (len(self.persons) > 0):
#                                print(self.persons)
                        else:
                            self.persons = {}

                        self.publish_sensor_data()
                        self.send_debug(b'***'+byte)
                        synchronized = False
        except Exception as e:
            print("Error!")
            print(e)
            self.stop()
            print("Quitting")


if __name__ == "__main__":
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    grideye = GrideyePeoplecounter(sys.argv[1])

    def signal_handler(signal, frame):
        def run(*args):
            print('Stopping sensor...')
            grideye.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    Thread(target=grideye.start).start()
