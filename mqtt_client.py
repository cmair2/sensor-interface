#!/usr/bin/env python3

import json
import uuid
import paho.mqtt.client as mqtt
import time
import socket
import sys
import os
import numpy as np
from configupdater import ConfigUpdater
from zeroconf import Zeroconf
from typing import cast


TYPE = '_mqtt._tcp.local.'
NAME = 'Mosquitto MQTT server on rpi-sensor'

class MQTTClient():

    def __init__(self, name, configfile=None):
        self.subscriptions = []
        self.name = name
        if configfile:
            self._configfile = configfile
        else:
            self._configfile = 'config2.cfg'

        self.config = ConfigUpdater()
        print('Using config file: {}'.format(self._configfile))
        self.config.read(self._configfile)

        self._load_mqtt_config()

        self.mqtt_client = mqtt.Client()
        self.mqtt_client.on_connect = self.on_mqtt_connect
        self.mqtt_client.on_message = self.on_mqtt_message

        if (self._mqtt_autodiscover):
            self.zeroconf = Zeroconf()
            try:
                info = self.zeroconf.get_service_info(TYPE, NAME + '.' + TYPE)
                if info:
                    self._mqtt_host = socket.inet_ntoa(cast(bytes, info.address))
                    self._mqtt_port = cast(int, info.port)
                    print("Using autodiscovered mqtt server:")
                    print("  Address: %s:%d" % (self._mqtt_host, self._mqtt_port))
                    print("  Weight: %d, priority: %d" % (info.weight, info.priority))
                    print("  Server: %s" % (info.server,))
                    if info.properties:
                         print("  Properties are:")
                         for key, value in info.properties.items():
                              print("    %s: %s" % (key, value))
                    else:
                        print("  No properties")
                else:
                    print("  No info")
                print('\n')
            finally:
                self.zeroconf.close()

        self.mqtt_client.username_pw_set(self._mqtt_user, self._mqtt_pass)
        self.mqtt_client.connect_async(self._mqtt_host, self._mqtt_port)

    def _load_mqtt_config(self):
        self._mqtt_host = self.load_config_value('MQTT', 'host', default='localhost')
        self._mqtt_port = int(self.load_config_value('MQTT', 'port', default='1883'))
        self._mqtt_user = self.load_config_value('MQTT', 'username', default='test')
        self._mqtt_pass = self.load_config_value('MQTT', 'password', default='test')
        self._mqtt_autodiscover = self.load_config_value('MQTT', 'autodiscover', default='true') == "true"

    def load_config_value(self, section, name, *, default='', create_if_missing=True):
        if not self.config.has_section(section) and create_if_missing:
            self.config.add_section(section)
            self.config[section].add_before.space()
            self.save_config()

        if self.config.has_option(section, name):
            return self.config.get(section, name).value
        elif create_if_missing:
            self.config.set(section, name, default)
            self.save_config()
            return self.config.get(section, name).value
        else:
            return None

    def save_config(self):
        self.config.update_file()

    def on_mqtt_connect(self, client, userdata, flags, rc):
        print('Connected to MQTT server {} with result code {}.'.format(self._mqtt_host, rc))

    def on_mqtt_message(self, client, userdata, msg):
        print("Got: " + msg.topic+" "+str(msg.payload))

    def mqtt_start(self):
        self.mqtt_client.loop_start()

    def mqtt_stop(self):
        self.mqtt_client.loop_stop()

    def publish(self, payload):
        pass
