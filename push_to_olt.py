import json
import logging
import numpy as np
import os
import random
import signal
import sys
import time
import uuid
import utils
from threading import Timer
from mqtt_client import MQTTClient
import paho.mqtt.client as mqtt

class Lightelligence(MQTTClient):

    def __init__(self, name, configfile=None):
        MQTTClient.__init__(self, name, configfile)
        self.lightelligence_config = {}
        self.load_config(name)
#        self.olt_client = mqtt.Client(client_id='#MCC-Raum-W.02.62-Gateway_{}'.format(random.randint(0, 1024)), protocol=mqtt.MQTTv311)
        self.olt_client = mqtt.Client(client_id='Gateway_{}'.format(random.randint(0, 1024)), protocol=mqtt.MQTTv311)
        self.olt_client.tls_set(ca_certs=self._ca_certs, certfile=self._certfile, keyfile=self._keyfile)
        self.olt_client.on_connect = self.on_lightelligence_mqtt_connect
        self.olt_client.on_disconnect = self.on_lightelligence_mqtt_disconnect
        self.olt_client.on_message = self.on_lightelligence_mqtt_message
        self.olt_client.connect_async(self._host, self._port)

    def load_config(self, name):
        self._uuid = self.load_config_value(self.name, 'uuid', default=uuid.uuid4())
        self._host = self.load_config_value(self.name, 'host', default='mqtt.preview.oltd.de')
        self._port = int(self.load_config_value(self.name, 'port', default='8883'))
        self._ca_certs = self.load_config_value(self.name, 'ca_certs', default='chain.pem')
        self._certfile = self.load_config_value(self.name, 'certfile', default='device_cert.pem')
        self._keyfile = self.load_config_value(self.name, 'keyfile', default='device_key.pem')
        for sensor_name in self.config[name].options():
            if sensor_name != 'uuid':   # Do not try to load own uuid
                sensor_olt_id = self.load_config_value(self.name, sensor_name, create_if_missing=False)
                if utils.is_valid_uuid(sensor_olt_id):   # Check for valid uuid and ignore all other config options
                    sensor_uuid = self.load_config_value(sensor_name, 'uuid')
                    self.lightelligence_config[sensor_uuid] = {
                        'name': sensor_name,
                        'olt_id': sensor_olt_id,
                        'type': self.load_config_value(sensor_name, 'type')
                    }
                    print('Loaded sensor {} of type {} with LIGHTELLIGENCE ID {}'.format(sensor_name, self.lightelligence_config[sensor_uuid]['type'], sensor_olt_id))

    def on_mqtt_connect(self, client, userdata, flags, rc):
        print('HARRRRRR Connected to MQTT server {} with result code {}.'.format(self._mqtt_host, rc))
        for sensor_id in self.lightelligence_config:
            sensor = self.lightelligence_config[sensor_id]
            self.mqtt_client.subscribe('sensor/' + sensor['type'] + '/' + sensor_id)
            print('subscribing to sensor/' + sensor['type'] + '/' + sensor_id)

    def on_mqtt_message(self, client, userdata, msg):
        print("HARRRRR Got: " + msg.topic) #+" " + str(msg.payload))
        s = json.loads(msg.payload.decode('UTF-8'))
        payload = {}
        topic = msg.topic.split('/')
        if topic[0] == 'sensor':
            if topic[1] == self.lightelligence_config[topic[2]]['type'] == 'peoplecounter_absolute':
                if not 'coords' in s:
                    return
                coords = [s['coords'][i] for i in s['coords']]
                payload = {
                    'deviceId': self.lightelligence_config[topic[2]]['olt_id'],
                    'type': 'attributes',
                    'value': {
                        'count': len(coords),
                        'coordinates': coords
                    }
                }
                self.publish_to_lightelligence(payload)
                print(payload)


    def publish_to_lightelligence(self, msg):
        self.olt_client.publish('data-ingest', payload=json.dumps(msg).encode('UTF-8'))


    def on_lightelligence_mqtt_connect(self, client, userdata, flags, rc):
        print('{}: Connected to LIGHTELLIGENCE MQTT server {} with result code {}.'.format(time.time(), self._mqtt_host, rc))

    def on_lightelligence_mqtt_disconnect(self, client, userdata, rc):
        print("{}: Disconnected from LIGHTELLIGENCE with code {} ({})".format(time.time(), mqtt.connack_string(rc), rc))

    def on_lightelligence_mqtt_message(self, client, userdata, msg):
        print("{}: Got from LIGHTELLIGENCE: {} {}".format(time.time(), msg.topic, str(msg.payload)))


    def start(self):
        self.olt_client.loop_start()
        self.mqtt_start()
        while True:
            time.sleep(1)

    def stop(self):
        print("Stopping..")
        self.mqtt_stop()
        self.olt_client.loop_stop()



if __name__ == '__main__':
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    ptolt = Lightelligence(sys.argv[1])

#    def signal_handler(signal_frame, nope):
#        def run(*args):
#            print('Stopping Geofence')
#            geofence.stop()
#        Thread(target=run).start()

    signal.signal(signal.SIGINT, quit)

    ptolt.start()
