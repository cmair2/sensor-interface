import json
import signal
import serial
import sys
import numpy as np
from threading import Thread, Timer
from generic_sensor import GenericSensor
from ruuvitag_sensor.ruuvi_rx import RuuviTagReactive


class RuuvitagEnvironmentalSensor(GenericSensor):

    sensor_type = 'ambientair'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self._stop_recv_timeout_timer = False
        self._recv_timeout_timer = None
        self._recv_timeout = None
        self._ruuvi_rx = RuuviTagReactive()
        self.load_config()

    def load_config(self):
        changed = False
        if self.config.has_option(self.name, 'recv_timeout'):
            self._recv_timeout = int(self.config.get(self.name, 'recv_timeout').value)
        else:
            self.config.set(self.name, 'recv_timeout', '20')
            changed = True
        if changed:
            self.save_config()
            self.load_config()

    def handle_data(self, data):
        self._recv_timeout_timer.cancel()
        self._recv_timeout_timer = Timer(self._recv_timeout, quit)
        self._recv_timeout_timer.start()
        msg = {
            'acceleration': {
                    'x': data[1]['acceleration_x'],
                    'y': data[1]['acceleration_y'],
                    'z': data[1]['acceleration_z'],
                },
            'temperature': data[1]['temperature'],
            'humidity': data[1]['humidity'],
            'pressure': data[1]['pressure'],
            'battery': data[1]['battery']
        }
        self.publish(msg)
        print("Tag: {}: {}".format(data[0], msg))


    def start(self):
        self.mqtt_start()
        self._recv_timeout_timer = Timer(self._recv_timeout, quit)
        self._ruuvi_rx.get_subject().subscribe(self.handle_data)

    def stop(self):
        self._recv_timeout_timer.cancel()
        try:
            self._ruuvi_rx.stop()
        except Exception as e:
            print(e)
        self.mqtt_stop()
        print("Stopped. Ignore the previous error.")

if __name__ == "__main__":
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    ruuvi = RuuvitagEnvironmentalSensor(sys.argv[1])

    def signal_handler(signal, frame):
        def run(*args):
            print('Stopping sensor...')
            ruuvi.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    ruuvi.start()
