#!/usr/local/bin/python3

import os
import json
from threading import Timer
import serial
import struct
import uuid

import threading
import time
import paho.mqtt.client as mqtt
import configparser
import numpy as np



config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')
SERIALPORT = config.get('MLX90640', 'SERIALPORT',   fallback='/dev/ttyACM3')
BAUDRATE = config.get('MLX90640', 'BAUDRATE',   fallback='115200')


normalize = 32
synchronized = False

global persons
sensor_topic = "sensor/peoplecounter_absolute/mlx90640"


client = mqtt.Client()
localclient = mqtt.Client()

quit = False

def timeout():
    global persons
    coords = {}
    persons = {x: persons[x] for x in persons if persons[x]['last_seen']+3 > time.time() }
    for p in persons:
        coords[p] = { k: v for k, v in persons[p].items() if k != 'last_seen' }
    msg = {"timestamp": time.time(), "count": len(persons), "coords": coords}
    client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
    print(msg)
    if not quit:
        t = Timer(3, timeout)
        t.start()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    pass

def on_mqtt_message(client, userdata, msg):
    pass


if __name__ == "__main__":
    global persons
    persons = {}
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)
    client.loop_start()

    t = Timer(3, timeout)
    t.start()

    try:
        with serial.Serial(SERIALPORT, BAUDRATE) as ser:
            while True:
                line = ser.readline().decode('ascii').strip()
                print(line)
                if line.startswith('OBJ'):
                    try:
                        obj, size, cx, cy = line.split(',')
                        id = obj.split(' ')[1]
                        x = float(cx.split('=')[1]) / normalize
                        y = float(cy.split('=')[1]) / normalize
                        persons[id] = {'x': x, 'y': y, 'last_seen': time.time()}
                        coords = {}
                        for p in persons:
                            coords[p] = { k: v for k, v in persons[p].items() if k != 'last_seen' }

                        msg = {
                            "timestamp": time.time(),
                            "count": len(persons),
                            "coords": coords,
                        }
                        client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
                        print(msg)
                    except Exception as e:
                        print(e)
#                    client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
    except Exception as e:
        print("Error!")
        print(e)
        client.loop_stop()
        quit = True
        t.cancel()


    client.loop_stop()
