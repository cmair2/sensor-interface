#!/usr/bin/python3

import json
import time
import paho.mqtt.client as mqtt
from libpurecoollink.dyson import DysonAccount
from libpurecoollink.const import FanSpeed, FanMode, NightMode, Oscillation, \
    FanState, StandbyMonitoring, QualityTarget
from libpurecoollink.dyson_pure_state import DysonPureHotCoolState, \
  DysonPureCoolState, DysonEnvironmentalSensorState


MQTTHOST = 'm23.cloudmqtt.com'      # The remote host
MQTTPORT = 16856                    # The same port as used by the server
USERNAME = "testuser"
PASSWORD = "test"


devices = []
client = mqtt.Client()


def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("ba/fan/#")


def on_mqtt_message(client, userdata, msg):
#    print("Got: " + msg.topic+" "+str(msg.payload))
    s = json.loads(str(msg.payload.decode('UTF-8')))
    print(s)
    if (s['command'] == 'set_fan'):
        config = {}
        if 'mode' in s:
            if s['mode'] == "ON":
                config['fan_mode'] = FanMode.FAN
            elif s['mode'] == "OFF":
                config['fan_mode'] = FanMode.OFF
            elif s['mode'] == "AUTO":
                config['fan_mode'] = FanMode.AUTO
        if 'speed' in s:
            if s['speed'] == "AUTO":
                config['fan_speed'] = FanSpeed.FAN_SPEED_AUTO
            elif s['speed'] == '1':
                config['fan_speed'] = FanSpeed.FAN_SPEED_1
            elif s['speed'] == '2':
                config['fan_speed'] = FanSpeed.FAN_SPEED_2
            elif s['speed'] == '3':
                config['fan_speed'] = FanSpeed.FAN_SPEED_3
            elif s['speed'] == '4':
                config['fan_speed'] = FanSpeed.FAN_SPEED_4
            elif s['speed'] == '5':
                config['fan_speed'] = FanSpeed.FAN_SPEED_5
            elif s['speed'] == '6':
                config['fan_speed'] = FanSpeed.FAN_SPEED_6
            elif s['speed'] == '7':
                config['fan_speed'] = FanSpeed.FAN_SPEED_7
            elif s['speed'] == '8':
                config['fan_speed'] = FanSpeed.FAN_SPEED_8
            elif s['speed'] == '9':
                config['fan_speed'] = FanSpeed.FAN_SPEED_9
            elif s['speed'] == '10':
                config['fan_speed'] = FanSpeed.FAN_SPEED_10
        if 'oscillation' in s:
            if s['oscillation'] == "ON":
                config['oscillation'] = Oscillation.OSCILLATION_ON
            elif s['oscillation'] == "OFF":
                config['oscillation'] = Oscillation.OSCILLATION_OFF

        print(config)
        devices[0].set_configuration(**config)


# ... connection do dyson account and to device ... #
def on_message(msg):
    # Message received
    if isinstance(msg, DysonPureCoolState):
        # Will be true for DysonPureHotCoolState too.
        print("DysonPureCoolState message received")
        message = {"command": "status",
                   "timestamp" : int(time.time()),
                   "deviceID": devices[0].serial,
                   "location": "DE",
                   "mode": msg.fan_mode,
                   "state": msg.fan_state,
                   "speed": msg.speed,
                   "oscillation": msg.oscillation
                  }
        client.publish("ba/fan", payload=json.dumps(message).encode('UTF-8'))
    if isinstance(msg, DysonPureHotCoolState):
        print("DysonPureHotCoolState message received")
    if isinstance(msg, DysonEnvironmentalSensorState):
        print("DysonEnvironmentalSensorState received")
    print(msg)


print("Connecting to Dyson cloud..")
# Log to Dyson account
# Language is a two characters code (eg: FR)
dyson_account = DysonAccount("C.Mair2@osram.com","dyson10t","DE")

logged = None
while not logged:
    try:
        logged = dyson_account.login()
        #print(dyson_account)
        if not logged:
            print('Unable to login to Dyson account')
#            exit(1)
    except Exception as e:
        print(e)
        time.sleep(2)
print("Connected.")

# List devices available on the Dyson account
devices = dyson_account.devices()

print(devices)
#print(devices[0])

print("Connecting to fan..")
connected = None
while not connected:
    # Connect using discovery to the first device
    try:
        connected = devices[0].connect("192.168.42.52")
        #print(connected)
    except Exception as e:
        print(e)
        time.sleep(1)

print("Connected")
# connected == device available, state values are available, sensor values are available

devices[0].add_message_listener(on_message)

#print(devices[0].serial)
#print(devices[0].credentials)
#print(devices[0].command_topic)
#print(devices[0].network_device)
#print(devices[0].status_topic)

# Turn on the fan to speed 2
devices[0].set_configuration(fan_mode=FanMode.FAN, fan_speed=FanSpeed.FAN_SPEED_2)

# Turn on oscillation
devices[0].set_configuration(oscillation=Oscillation.OSCILLATION_ON)

#print(devices[0].state.speed)
#print(devices[0].state.oscillation)
#print(devices[0].environmental_state.humidity)
#print(devices[0].environmental_state.sleep_timer)




#devices[0].connect()

client.on_connect = on_mqtt_connect
client.on_message = on_mqtt_message

client.username_pw_set(USERNAME, PASSWORD)

client.connect_async(MQTTHOST, MQTTPORT)

client.loop_start()

while True:
    time.sleep(1)


client.loop_stop()
# Disconnect
devices[0].disconnect()
