#!/usr/bin/python3

import base64
import json
import time
import paho.mqtt.client as mqtt
import subprocess
from threading import Timer
import os
import signal
import re
import sys
import configparser



config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')


# Disable
#return -1

devices = []
client = mqtt.Client()
pid = {}


def getPIDs():
    completed = subprocess.run('/usr/bin/screen -wipe', shell=True, stdout=subprocess.PIPE)
    completed = subprocess.run('/usr/bin/screen -ls', shell=True, stdout=subprocess.PIPE)
    stdout = completed.stdout.decode('UTF-8')
    info = re.findall('(\\d*)(\.)(.*)(\\t\(\\d.*)', stdout)
    pids = dict([(x[2], int(x[0])) for x in info if 'watchdog' not in x[2]])    # Ignore watchdog process
    return pids


def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("system/#")


def start_missing_process():
    pid = getPIDs()
    for name, command in config.items("WATCHDOG"):
        if name not in pid:
            print("Starting {}".format(name))
            cmd = ["/usr/bin/screen", "-d", "-m", "-S", "{}".format(name), "/usr/bin/python3", "{}/{}".format(path, command)]
#            print(cmd)
            subprocess.Popen(cmd)
    subprocess.run(['/usr/bin/screen', '-wipe'], stdout=subprocess.DEVNULL)
#    print(getPIDs())


def killall_processes():
    pids = getPIDs()
    print(pids)
    for daemon in pids:
        print(daemon)
        os.kill(pids[daemon], signal.SIGKILL)
    subprocess.run(['/usr/bin/screen', '-wipe'])


def on_mqtt_message(client, userdata, msg):
#    print("Got: " + msg.topic+" "+str(msg.payload))
    s = json.loads(str(msg.payload.decode('UTF-8')))
    print(s)
    if 'command' in s and ((s['command'] == 'reboot') or (s['command'] == 'restart-services')):
        killall_processes()
        message =  {'component': 'christoph-pi', 'status': 'restart-services', 'percentage': 0}
        client.publish("system", payload=json.dumps(message).encode('UTF-8'))
        start_missing_process()


    elif 'command' in s and (s['command'] == 'reboot-system'):
#        pids = getPIDs()
#        for x in pids:
#            if x[1] != os.path.basename(sys.argv[0]):
#                os.kill(x[0], signal.SIGKILL)
        message =  {'component': 'christoph-pi', 'status': 'reboot-system', 'percentage': 0}
        client.publish("system", payload=json.dumps(message).encode('UTF-8'))
        subprocess.run('sudo /sbin/reboot', shell=True)

t = None


def timer_timeout():
    pid = getPIDs()
    target = [name for name, command in config.items("WATCHDOG")]
    reality = [name for name in pid]
    missing = list(set(target) - set(reality))
    if len(pid) > 0:
        percentage = int(len(reality) * 1000 / len(target))/10
    else:
        percentage = 0
    if len(missing) == 0:
        status = "OK"
    else:
        status = "Error"
    message = {'component': 'christoph-pi', 'status': status, 'percentage': percentage, 'details': missing}
    client.publish("system", payload=json.dumps(message).encode('UTF-8'))
    t = Timer(3, timer_timeout)
    t.start()
    if len(missing) != 0:
        start_missing_process()



t = Timer(3, timer_timeout)
t.start()

client.on_connect = on_mqtt_connect
client.on_message = on_mqtt_message

client.username_pw_set(USERNAME, PASSWORD)

client.connect_async(MQTTHOST, MQTTPORT)

start_missing_process()

client.loop_start()

while True:
    time.sleep(1)

client.loop_stop()
