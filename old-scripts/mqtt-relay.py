#!/usr/bin/python3

import os
import json
import numpy as np
import paho.mqtt.client as mqtt
import socket
import struct
import time
import configparser


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

LOCAL_MQTTHOST = config.get('MQTT',    'HOST',     fallback='127.0.0.1') # The remote host
LOCAL_MQTTPORT = config.getint('MQTT', 'PORT',     fallback=1883)         # The same port as used by the server
LOCAL_USERNAME = config.get('MQTT',    'USERNAME', fallback='')
LOCAL_PASSWORD = config.get('MQTT',    'PASSWORD', fallback='')

CLOUD_MQTTHOST = config.get('CLOUDMQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
CLOUD_MQTTPORT = config.getint('CLOUDMQTT', 'PORT',     fallback=16856)               # The same port as used by the server
CLOUD_USERNAME = config.get('CLOUDMQTT',    'USERNAME', fallback='testuser')
CLOUD_PASSWORD = config.get('CLOUDMQTT',    'PASSWORD', fallback='test')



def cloud_on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # We only care about commands sent to us.
    client.subscribe("system/#")

def local_on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("#")


def cloud_on_message(client, userdata, msg):
    s = json.loads(str(msg.payload.decode('UTF-8')))
    print("Got from cloud: " + str(s))
    # Do not relay status messages echoed back from the cloud. This would create a loop!
    if 'status' not in s:
        print("Sending to local: " + msg.topic + " " + str(msg.payload))
        local_client.publish(msg.topic, payload=msg.payload.decode('UTF-8'))

def local_on_message(client, userdata, msg):
    s = json.loads(str(msg.payload.decode('UTF-8')))
#    print("Got from local: " + str(s))
    # Do not relay command messages echoed back from the local side. This would create a loop!
    if 'command' not in s:
        payload = json.loads(msg.payload.decode('UTF-8'))
        print(payload.keys())
        p = {k: v for k, v in payload.items() if k not in ['backgroundImage', 'raw', 'rawData']}
        print('\n')
        print(p)
        print("Sending to cloud: " + msg.topic + " " + str(p))
        cloud_client.publish(msg.topic, payload=json.dumps(p))


cloud_client = mqtt.Client()
cloud_client.on_connect = cloud_on_connect
cloud_client.on_message = cloud_on_message

cloud_client.username_pw_set(CLOUD_USERNAME, CLOUD_PASSWORD)
cloud_client.connect_async(CLOUD_MQTTHOST, CLOUD_MQTTPORT)
cloud_client.loop_start()


local_client = mqtt.Client()
local_client.on_connect = local_on_connect
local_client.on_message = local_on_message

local_client.username_pw_set(LOCAL_USERNAME, LOCAL_PASSWORD)
local_client.connect_async(LOCAL_MQTTHOST, LOCAL_MQTTPORT)
local_client.loop_start()

while True:
    time.sleep(1)

cloud_client.disconnect()
cloud_client.loop_stop()

local_client.disconnect()
local_client.loop_stop()
