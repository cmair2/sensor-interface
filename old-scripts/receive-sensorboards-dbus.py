#!/usr/bin/python3

import gatt
import selectors
import signal
import socket
import sys
import time
import threading

devices = dict()

sel = selectors.DefaultSelector()
clients = list()

connections = []
connection_threads = []

service_uuids=[]
#    'bd10',
#    '0000bd20-0000-1000-8000-00805f9b34fb',
#    '0000bd30-0000-1000-8000-00805f9b34fb',
#    '0000bd40-0000-1000-8000-00805f9b34fb',
#    '0000bd50-0000-1000-8000-00805f9b34fb'
#]


class BLEDeviceManager(gatt.DeviceManager):
    running = False

    def make_device(self, mac_address):
        print("Make device " + mac_address)
        return SensorBoard(mac_address=mac_address, manager=self)

    def device_discovered(self, device):
#        print("Discovered %s" % device.alias())
        name = device.alias()
        if isinstance(name, str) and name.startswith("LMS-Sens-Board"):
            if device.mac_address not in devices:
                print("Connecting to [%s] %s" % (device.mac_address, device.alias()))
                print(devices)
                thread = threading.Thread(target = device.connect)
                devices[device.mac_address] = (device, thread)
                thread.start()
#                device.connect()
            else:
                print("Ignoring discovered device [%s] %s" % (device.mac_address, device.alias()))

    def run(self):
        print("Starting manager.")
        self.running = True
        super().start_discovery(service_uuids)
        try:
           super().run()
        except KeyboardInterrupt:
            print("Kbd int in manager")
            self.quit()
        except Exception as e:
            print(e)
        print("Manager stopped.")

    def quit(self):
        for d in devices:
            devices[d][0].disconnect()
        print("Stopping manager.")
        super().stop_discovery()
        super().stop()
        self.running = False
        print("Done.")


class SensorBoard(gatt.Device):
    reconnect = False

    def connect(self):
        super().connect()

    def connect_succeeded(self):
        super().connect_succeeded()
        print("[%s] Connected" % (self.mac_address))

    def connect_failed(self, error):
        super().connect_failed(error)
        print("[%s] Connection failed: %s" % (self.mac_address, str(error)))
#        del devices[self.mac_address]
        self.disconnect()
        self.connect()

    def disconnect(self, reconnect=True):
        self.reconnect = reconnect
        super().disconnect()

    def disconnect_succeeded(self):
        super().disconnect_succeeded()
        print("[%s] Disconnected" % (self.mac_address))
        del devices[self.mac_address]
        if self.reconnect:
            self.connect()

    def services_resolved(self):
        super().services_resolved()

#        print("[%s] Resolved services" % (self.mac_address))
        for service in self.services:
#            print("[%s]  Service [%s]" % (self.mac_address, service.uuid))
            for characteristic in service.characteristics:
#                print("[%s]    Characteristic [%s]" % (self.mac_address, characteristic.uuid))
                if (characteristic.uuid.startswith("0000bd") and not characteristic.uuid.startswith("0000bd12x")):
                    characteristic.enable_notifications(True)
#        print("Services resolved.")

    def characteristic_value_updated(self, characteristic, value):
        currenttime = int(time.time()*10)
        payload = b''
        id = int(characteristic.uuid[6:8], base=16)
        d = currenttime.to_bytes(8, byteorder='little') + value
        payload = id.to_bytes(1, byteorder='little') + len(d).to_bytes(1, byteorder='little') + d

        for conn in list(clients):
            try:
                conn.send(b'\nOSRAM' + self.mac_address.encode('ascii') + payload)
            except Exception as e:
                print("Could not send sensor data" + str(conn))
                conn.close()
                sel.unregister(conn)
                clients.remove(conn)
                print(e)
#        print(characteristic, value)


def accept(sock, mask):
    global sel
    global clients
    conn, addr = sock.accept()  # Should be ready
#    print('accepted', conn, 'from', addr)
    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, read)
    clients.append(conn)
#    print(clients)

def read(conn, mask):
#    print("read..")
    try:
        data = conn.recv(1000)  # Should be ready
        if data:
            print('got', repr(data), 'from', conn)
#            conn.send(data)  # Hope it won't block
        else:
#            print('closing', conn)
            sel.unregister(conn)
            conn.close()
            clients.remove(conn)
    except:
        print('closing', conn)
        sel.unregister(conn)
        conn.close()
        clients.remove(conn)


def signal_handler(signal, frame):
        print('You pressed Ctrl+C!')
        for d in devices:
            d.disconnect(reconnect=False)
        manager.stop()
        sys.exit(0)




signal.signal(signal.SIGINT, signal_handler)

try:
    print("Search for BLE Sensors...")
    manager = BLEDeviceManager(adapter_name='hci0')
    manager.start_discovery()

    thread = threading.Thread(target = manager.run)
    thread.start()

    print("Setup Network...")

    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('0.0.0.0', 10000))
    sock.listen(100)
    sock.setblocking(False)
    sel.register(sock, selectors.EVENT_READ, accept)

    print("Listening...")
    try:
        while manager.running:
            events = sel.select(1)
            for key, mask in events:
                callback = key.data
                callback(key.fileobj, mask)

    except Exception as e:
        print("Terminate: " + str(e))
#        sock.close()
        for conn in clients:
            conn.close()


except Exception as e:
    print(e)
    signal_handler(None, None)
