import os
import json
import numpy as np
import paho.mqtt.client as mqtt
import socket
import struct
import time
import configparser
import psycopg2
import datetime
from threading import Timer
from PIL import Image


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

DATABASE = config.get('DATABASE', 'DATABASE', fallback='sensordb')
DBHOST   = config.get('DATABASE', 'DBHOST',   fallback='192.168.2.10')
DBPORT   = config.get('DATABASE', 'DBPORT',   fallback=5432)
DBUSER   = config.get('DATABASE', 'DBUSER',   fallback='mqtt')
DBPWD    = config.get('DATABASE', 'DBPWD',    fallback='SensorDatabas3P@ßßword')

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')


conn = psycopg2.connect(dbname=DATABASE, user=DBUSER, password=DBPWD, host=DBHOST, port=DBPORT)

with conn:
    with conn.cursor() as cur:
        cur.execute("CREATE TABLE IF NOT EXISTS images (id serial PRIMARY KEY, ts timestamp, raw numeric[]);")
        cur.execute("CREATE TABLE IF NOT EXISTS people (id serial PRIMARY KEY, device varchar(30), ts timestamp, coords numeric[2][], raw_id integer REFERENCES images (id) ON DELETE RESTRICT);")
        cur.execute("CREATE TABLE IF NOT EXISTS transforms (id serial PRIMARY KEY, device varchar(30) UNIQUE, a numeric, b numeric, c numeric, d numeric, tx numeric, ty numeric);")



stop = False


sensors = {}

t = None



def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("sensor/#")


def on_message(client, userdata, msg):
    sensors[msg.topic] = msg



def timeout():
    global stop
#    print("Got: " + msg.topic+" || "+str(msg.payload.decode('UTF-8')))
#    print(s)
    detected = 0
    sensorvalues = list(sensors.items())
    for k, msg in sensorvalues:
        s = json.loads(msg.payload.decode('UTF-8'))
        if "/transform" not in msg.topic:
            if "grideye" not in msg.topic:
                if "coords" in s:
                    detected = detected + len(s['coords'])
                else:
                    print(s)
        else:
            detected = 1 # Always save transform matrices
    if detected > 0:
        ts_db = None
        for k, msg in sensorvalues:
            if 'camera' in k and 'transform' not in k:
                ts_db = datetime.datetime.fromtimestamp(json.loads(msg.payload.decode('UTF-8'))['timestamp'])
                print("using camera ts: {}".format(ts_db))
        if ts_db is None:
            ts_db = datetime.datetime.now()
        for k, msg in sensorvalues:
            del sensors[k]
#            print(sensors)
            s = json.loads(msg.payload.decode('UTF-8'))
            try:
                if "/transform" in msg.topic:
                    device = msg.topic.split("/")[-2]
                    transform = s
                    print("Transform: {}: {}".format(device, transform))
                    with conn:
                        with conn.cursor() as cur:
                            transform['device'] = device
                            cur.execute("INSERT INTO transforms (device, a, b, c, d, tx, ty) VALUES (%(device)s, %(a)s, %(b)s, %(c)s, %(d)s, %(tx)s, %(ty)s) \
                                     ON CONFLICT (device) DO UPDATE SET a=%(a)s, b=%(b)s, c=%(c)s, d=%(d)s, tx=%(tx)s, ty=%(ty)s where transforms.device like %(device)s;", transform)
                else:
                    with conn:
                        with conn.cursor() as cur:
                            device = msg.topic.split("/")[-1]
                            rawID = None
                            if 'rawData' in s and device in ['camera'] and device not in ['grideyekompakt']:
                                rawdata = bytes([item for row in s['rawData'] for item in row])
                                imgdata = Image.frombytes('L', (len(s['rawData'][0]), len(s['rawData'])), rawdata)
                                jpgdata = imgdata.tobytes('jpeg', 'L')
#                                print(list(jpgdata))
#                                print(len(rawdata))
                                cur.execute("INSERT INTO images (ts, raw) VALUES (%s, %s) RETURNING id;", (ts_db, list(jpgdata)))
                                rawID = cur.fetchone()[0]
                            if 'coords' in s:
                                coords = s['coords']
                                print("Data: {}: {}".format(device, coords))
                                cur.execute("INSERT INTO people (device, ts, coords, raw_id) VALUES (%s, %s, %s, %s);", (device, ts_db, str([[coords[i]['x'], coords[i]['y']] for i in coords]).replace('[', '{').replace(']', '}'), rawID))
                            else:
                                print(s)
            except Exception as e:
                print("Exception!!")
                print(e)
                stop = True
    else:
        print("Nobody there. Ignoring data at {}".format(datetime.datetime.now()))
    if not stop:
        t = Timer(1, timeout)
        t.start()


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(USERNAME, PASSWORD)

client.connect_async(MQTTHOST, MQTTPORT)

client.loop_start()

t = Timer(1, timeout)
t.start()


while not stop:
    time.sleep(1)

conn.close()
client.loop_stop()
client.disconnect()
