#!/usr/bin/python3

import os
import json
import numpy as np
import paho.mqtt.client as mqtt
import socket
import struct
import time
import configparser


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

HOST = config.get('SENSORSERVER',    'HOST', fallback='127.0.0.1')
PORT = config.getint('SENSORSERVER', 'PORT', fallback=10000)

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')

global stop
stop = False

HEADER_LENGTH = 6+17+8+1+1


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
#    client.subscribe("sensor/#")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("Got: " + msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.username_pw_set(USERNAME, PASSWORD)

client.connect_async(MQTTHOST, MQTTPORT)

client.loop_start()

while True:
    print("Connecting to %s:%s..." % (HOST, PORT))
    try:
        stop = False
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
#            s.settimeout(10)
            print("Connected.")
            data = b''
            while not stop:
                d = s.recv(4096)
                print(d)
                if (len(d) < 1):
                    stop = True
                    s = None
                    continue
                if len(d) > 3000:
                    print("Congestion warning while saving data!")
#                print("Got %d bytes" % (len(d)), end='')
                data = data + d
#                print(b'read: ' + data)
                while len(data) >= (HEADER_LENGTH):
#                    print(data[:HEADER_LENGTH])
                    header, mac, timestamp, msg, length = struct.unpack("<6s17sQBB", data[:HEADER_LENGTH])
#                    print(header, mac, timestamp, msg, length)
                    timestamp = timestamp / 1000
                    data = data[HEADER_LENGTH:]
#                    print(b'data: ' + data)

                    if len(data) >= length:
                        message = {
                            'timestamp': timestamp,
#                            'Ambient': { 'VOC': None, 'iaq': None, 'temperature': None, 'humidity': None, 'pressure': None },
#                            'Light': { 'clear': None, 'red': None, 'green': None, 'blue': None },
                        }
                        if msg == 0x11:
                            coords = struct.unpack("<{:d}H".format(int(length/2)), data[:length])
                            coo = [coords[i:i+2] for i in range(0, len(coords), 2)]
                            for x in range(0, len(coo), 1):
                                message['coords'][x] = {"x": x[0], "y": x[1]}
                            message['count'] = len(message['coords'])
                            client.publish("sensor/peoplecounter_absolute/"+mac.decode('ascii')+"_11", payload=json.dumps(message).encode('UTF-8'))

                        elif msg == 0x12:
                            print("Ignoring thermal image")
                        elif msg == 0x21:
                            event = struct.unpack("<B", data[:length])[0]
                        elif msg == 0x31:
                            n = struct.unpack("<H", data[:length])[0]
                            n = 20 * np.log10((126.19147441253433656304 * n / 1000000.0) / (2.0 * 0.00001))

# This won't get pushed anymore
#                        elif msg == 0x41:
#                            co2, status, resistance, voc = struct.unpack("<HBIH", data[:length])
#                            message['Ambient'] = {
#                                'VOC': voc,
#                                'CO2': co2
#                            }
#                            client.publish("sensor/"+mac.decode('ascii'), payload=json.dumps(message).encode('UTF-8'))
                        elif msg == 0x51:
                            clear, red, green, blue = struct.unpack("<HHHH", data[:length])
                            message['clear'] = round(clear, 2)
                            message['red'] = round(red, 2)
                            message['green'] = round(green, 2)
                            message['blue'] = round(blue, 2)
                            client.publish("sensor/color/"+mac.decode('ascii')+"_51", payload=json.dumps(message).encode('UTF-8'))
                        elif msg == 0x61:
                            iaq, temperature, humidity, pressure = struct.unpack("<ffff", data[:length])
                            message['iaq'] = round(iaq, 2)
                            message['temperature'] = round(temperature, 2)
                            message['humidity'] = round(humidity, 2)
                            message['pressure'] = round(pressure/100, 2)
                            client.publish("sensor/ambientair/"+mac.decode('ascii')+"_61", payload=json.dumps(message).encode('UTF-8'))

                        data = data[length:]
#                        print(b'data2: ' + 	data)
#                print(".")
    except Exception as e:
        print(e)
    print("Wait...")
    time.sleep(1)

client.disconnect()
client.loop_stop()
