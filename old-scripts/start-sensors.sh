#!/bin/sh

BASEDIR=/home/pi/DS-Sensors/sensorboard-viewer
cd $BASEDIR

# Start GUI
# QT_QPA_PLATFORM=linuxfb:fb=/dev/fb0 /usr/bin/screen -d -m -S sensor-gui.py /usr/bin/python3 ${BASEDIR}/sensor-gui.py

# Wait for 10 seconds to let all other services finish booting up.
echo "Waiting 10 seconds..."
sleep 10

# Start watchdog process
/usr/bin/screen -d -m -S watchdog-mqtt.py /usr/bin/python3 ${BASEDIR}/watchdog-mqtt.py
