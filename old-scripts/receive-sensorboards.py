#!/usr/bin/python3


import os
from bluepy.btle import Scanner, DefaultDelegate, Peripheral, UUID, BTLEException, ADDR_TYPE_RANDOM
import threading
import time
import struct
import sys
import selectors
import socket
import configparser


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

HOST = config.get('SENSORSERVER',    'BIND', fallback='0.0.0.0')
PORT = config.getint('SENSORSERVER', 'PORT', fallback=10000)


sel = selectors.DefaultSelector()
clients = list()

handleDatabase = dict()
characteristicBaseUUID = 0xbd00

connections = []
connection_threads = []
scanner = Scanner(0)


def accept(sock, mask):
    global sel
    global clients
    conn, addr = sock.accept()  # Should be ready
    print('accepted', conn, 'from', addr)
    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, read)
    clients.append(conn)
    print(clients)

def read(conn, mask):
    try:
        data = conn.recv(1000)  # Should be ready
        if data:
            print('got', repr(data), 'from', conn)
#            conn.send(data)  # Hope it won't block
    except Exception as e:
        print('closing', conn)
        try:
            sel.unregister(conn)
        except:
            print("Can't unregister conn")
        try:
            conn.close()
        except:
            print("Can't close conn")
        try:
            clients.remove(conn)
        except:
            print("Can't remove conn")


class NotificationDelegate(DefaultDelegate):

    def __init__(self, addr):
        DefaultDelegate.__init__(self)
        self.addr = addr

    def handleNotification(self, cHandle, data):
        currenttime = int(time.time()*1000)

        payload = b''

        id = int(handleDatabase[self.addr + "-" + str(cHandle)].uuid.getCommonName()[2:4], base=16)
#        print(str(id) + ": " + str(len(data)))
        payload = b'\nOSRAM'
        payload += self.addr.encode('ascii')
        payload += currenttime.to_bytes(8, byteorder='little')
        payload += id.to_bytes(1, byteorder='little')
        payload += len(data).to_bytes(1, byteorder='little')
        payload += data

        for conn in list(clients):
            try:
                conn.send(payload)
            except:
                print("Could not send sensor data")
                try:
                    sel.unregister(conn)
                except:
                    print("Could not unregister")
                try:
                    conn.close()
                except:
                    print("Could not close connection")
                try:
                    clients.remove(conn)
                except:
                    print("Can't remove conn")


class ConnectionHandlerThread (threading.Thread):
    def __init__(self, peripheral, addr):
        threading.Thread.__init__(self)
        self.peripheral = peripheral
        self.addr = addr
        self.stop_now = False

    def stop(self):
        self.stop_now = True

    def run(self):
        global connection_threads
        global handleDatabase
        try:
            self.peripheral.setMTU(247)
        except Exception as e:
            print("Could not set MTU!!")

        self.peripheral.withDelegate(NotificationDelegate(self.addr))
        errorcount = 0
        try:
            services = self.peripheral.getServices()
        except Exception as e:
            print("Could not get Services from device {} ({})! Disconnecting.".format(self.addr, e))
            services = []
            self.stop_now = True

        for s in [x for x in services if x.uuid.getCommonName().startswith("bd")]:
            print(s)
            time.sleep(1)
            ccc_desc = None
            ch = []
            try:
                ch = s.getCharacteristics()
            except Exception as e:
                print("Error getting characteristics from {} ({}). Closing connection.".format(self.addr, e))
                self.stop_now = True
            for c in ch:
                try:
                    print(c)
                    if c.uuid.getCommonName().startswith("bd12"):
                        continue
                    ccc_desc = c.getDescriptors(forUUID=0x2902)[0]
                    # Write "1" (uint16_t) to enable notifications or "0" to disable
                    handleDatabase[self.addr + "-" + str(c.getHandle())] = c
                    ccc_desc.write((1).to_bytes(2, byteorder='little'), True)  # "True" to wait for confirmation, aka make a "request". Without, it's a "command" which does nothing on the peripherals side.
                except Exception as e:
                    print("Error connecting to device {}".format(self.addr))
                    time.sleep(1)
                    errorcount = errorcount + 1
                    if errorcount < 10:
                        print("1# %s: Error: %s; retry (%d)..." % (self.addr, str(e), errorcount));
                    else:
                        print("1# %s: BLEException" % (self.addr))
                        print(str(e))
#                         connection_threads.remove(self)
                        break

        print("Connected to {}".format(self.addr))
        while errorcount < 1 and not self.stop_now:
            try:
                self.peripheral.waitForNotifications(1)
            except BTLEException:
                print("# %s: BLEException" % (self.addr))
                self.peripheral.disconnect()
#                connection_threads.remove(self)
#                errorcount = 7
                break;
        print("Disconnecting from %s\n" % self.addr)
        try:
            self.peripheral.disconnect()
        except Exception as e:
            print("Error during disconnect: {}".format(e))
            pass
        connection_threads.remove(self)
        for t in connection_threads:
            print(t.addr)
#        files.remove((self.connection_index, f))




class ScannerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print("Waiting for clients...")
        while True:
#            if len(clients) == 0:
#                for con in connection_threads:
#                    con.stop()
#                time.sleep(1)
#                continue
            devices = None
            try:
                devices = scanner.scan(2)
                for d in devices:
#                    print(d.addr + ": " + str(d.getScanData()))
                    try:
                        if (str(d.getValueText(9)).startswith("LMS-Sens-Board")):
                            p = Peripheral(d)
                            t = ConnectionHandlerThread(p, d.addr)
                            t.start()
                            connection_threads.append(t)
                            print('# Connected: ', str(len(connection_threads)))
                            for t in connection_threads:
                                print(t.addr)

                    except Exception as e:
                        print("Error connecting: {}".format(e))
            except Exception as e:
                print("Error connecting: {}".format(e))
                time.sleep(1)
        print("Scanner terminated.")




if __name__ == '__main__':
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((HOST, PORT))
    sock.listen(100)
    sock.setblocking(False)
    sel.register(sock, selectors.EVENT_READ, accept)

    try:
        s = ScannerThread()
        s.start()

        print("Listening...")
        while True:
            events = sel.select(1)
            for key, mask in events:
                callback = key.data
                callback(key.fileobj, mask)

    except Exception as e:
        print("Terminate: " + str(e))
        sock.close()
        for conn in clients:
            conn.close()
