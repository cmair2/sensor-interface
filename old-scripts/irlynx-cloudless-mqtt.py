#!/usr/local/bin/python3

import os
import json
from threading import Timer
import serial
import uuid

import threading
import time
import paho.mqtt.client as mqtt
import configparser
import numpy as np



config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

HOST = config.get('SENSORSERVER',    'HOST', fallback='127.0.0.1')
PORT = config.getint('SENSORSERVER', 'PORT', fallback=10000)

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')

SERIALPORT = config.get('IRLYNX', 'SERIALPORT', fallback='/dev/ttyUSB0')
BAUDRATE = config.get('IRLYNX',   'BAUDRATE', fallback=19200)

normalize = 80.0
synchronized = False

global persons
sensor_topic = "sensor/peoplecounter_absolute/irlynx"


client = mqtt.Client()
localclient = mqtt.Client()

stop_timer = False

def getRawImage():
    print("Get RAW image.")
    ser.write(b'\x52\x00')

def timeout():
    msg = {"timestamp": time.time(), "count": len(persons), "coords": persons}
    client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
    print(msg)
    if not stop_timer:
        t = Timer(3, timeout)
        t.start()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    pass

def on_mqtt_message(client, userdata, msg):
    pass


if __name__ == "__main__":
    global persons
    persons = {}
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)
    client.loop_start()

    t = Timer(5, timeout)
    t.start()
    rawImage = None

    rawTimer = Timer(3, getRawImage)
    rawTimer.start()


    try:
        with serial.Serial(SERIALPORT, BAUDRATE) as ser:
            ser.write(b'\x10\x00')
            ser.write(b'\x1a\x00')
            while True:
                byte = ser.read(1)
                if not synchronized:
                    if byte in [b'\x33', b'\x10', b'\x1a', b'\x52', b'\x53']:
                        synchronized = True
                if synchronized:
                    if byte in [b'\x10']:
                        length = int.from_bytes(ser.read(1), byteorder='little')
                        para = ser.read(length)
                        print("Serial:" + str(para))
                    elif byte in [b'\x1a']:
                        length = int.from_bytes(ser.read(1), byteorder='little')
                        para = ser.read(length)
                        print("FW-Version:" + str(para))
                    elif byte in [b'\x52']:
                        length = int.from_bytes(ser.read(1), byteorder='little')
                        para = ser.read(length)
                        if para == b'OK':
                            print("RAW image incoming...")
                    elif byte in [b'\x53']:
                        length = int.from_bytes(ser.read(1), byteorder='little')
                        linenumber = int.from_bytes(ser.read(1), byteorder='little')
#                        print("RAW #{}".format(linenumber))
                        data = ser.read(length-1)
                        if linenumber == 0:
                            rawImage = data
                        else:
                            rawImage = rawImage + data
                        if linenumber == 39:
                            img = np.frombuffer(rawImage, dtype=np.uint8).reshape((40, 40))
                            img = np.rot90(img)
                            img = np.flipud(img)
                            msg = {"timestamp": time.time(), "count": len(persons), "coords": persons, "raw": img.tolist()}
                            client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
                            print("push raw data")
                            if not stop_timer:
                                rawTimer = Timer(3, getRawImage)
                                rawTimer.start()
                    elif byte in [b'\x33', b'\x34', b'\x35', b'\x36', b'\x22', b'\x23']:
                        length = int.from_bytes(ser.read(1), byteorder='little')
                        para = ser.read(length)
#                        print(str(len(para)) + ":" + str(para))
                        if byte == b'\x33':
                            id = int(para[0])
                            x = min(float(para[2]) / normalize, 1.0)
                            y = min(float(para[3]) / normalize, 1.0)
 #                           if x not in persons.values():
                            persons[id] = {'x': x, 'y': y}
 #                           else:
 #                               print("Ignoring double counting")
#                            print((x, y))
                        if byte in [b'\34', b'\x35', b'\x36']:
                            id = int(para[0])
#                            print("Leave ", id)
                            if id in persons:
                                persons.pop(id)
                        if byte == b'\x22':
                             presence = int(para[0])
                             if presence == 0:
                                persons.clear()
#                                print("Presence ", presence)
                        if byte == b'\x23':
                            n = int(para[0])
                            if n == 0:
                                persons.clear()
#                            print("Number of people ", n)
#                            print(persons)
#                        print(persons)
                        msg = {"timestamp": time.time(), "count": len(persons), "coords": persons}
                        print(msg)
                        client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
                    else:
                        synchronized = False
                        print("sync-problem")
    except Exception as e:
        print("Error!")
        print(e)
        client.loop_stop()
        stop_timer = True
        t.cancel()


    client.loop_stop()
