#!/usr/local/bin/python3

import os
import json
from threading import Timer
import serial
import struct
import uuid
import selectors
import socket

import threading
import time
import paho.mqtt.client as mqtt
import configparser
import numpy as np
import binascii



config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')
SERIALPORT = config.get('GRIDEYE', 'SERIALPORT',   fallback='/dev/ttyACM1')
BAUDRATE = config.get('GRIDEYE', 'BAUDRATE',   fallback='115200')

DEBUGHOST = config.get('GRIDEYE', 'DEBUGHOST', fallback='0.0.0.0')
DEBUGPORT = config.get('GRIDEYE', 'DEBUGPORT', fallback=42002)

normalize = 3840
synchronized = False

global persons
sensor_topic = "sensor/peoplecounter_absolute/grideyekompakt"


client = mqtt.Client()
localclient = mqtt.Client()

global quit
quit = False

def timeout():
    global quit
    msg = {"timestamp": int(time.time()), "count": len(persons), "coords": persons}
    client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
    print(msg)
    if not quit:
        t = Timer(3, timeout)
        t.start()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    pass

def on_mqtt_message(client, userdata, msg):
    pass



sel = selectors.DefaultSelector()
clients = list()

def accept(sock, mask):
    global sel
    global clients
    conn, addr = sock.accept()  # Should be ready
    print('accepted', conn, 'from', addr)
    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, read)
    clients.append(conn)
    print(clients)

def read(conn, mask):
    try:
        data = conn.recv(1000)  # Should be ready
        if data:
            print('got', repr(data), 'from', conn)
#            conn.send(data)  # Hope it won't block
    except Exception as e:
        print('closing', conn)
        try:
            sel.unregister(conn)
        except:
            print("Can't unregister conn")
        try:
            conn.close()
        except:
            print("Can't close conn")
        try:
            clients.remove(conn)
        except:
            print("Can't remove conn")

def debug_listener():
    global quit
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((DEBUGHOST, DEBUGPORT))
    sock.listen(100)
    sock.setblocking(False)
    sel.register(sock, selectors.EVENT_READ, accept)

    print("Listening...")
    try:
        while not quit:
            events = sel.select(1)
            for key, mask in events:
                callback = key.data
                callback(key.fileobj, mask)

    except Exception as e:
        print("Terminate: " + str(e))
        sock.close()
        for conn in clients:
            conn.close()


def send_debug(payload):
    for conn in list(clients):
        try:
            conn.send(payload)
        except:
            print("Could not send sensor data")
            try:
                sel.unregister(conn)
            except:
                print("Could not unregister")
            try:
                conn.close()
            except:
                print("Could not close connection")
            try:
                clients.remove(conn)
            except:
                print("Can't remove conn")


if __name__ == "__main__":
    global persons
    persons = {}
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)
    client.loop_start()

    tcpserver = threading.Thread(target=debug_listener)
    tcpserver.start()

#    t = Timer(3, timeout)
#    t.start()

    try:
        with serial.Serial(SERIALPORT, BAUDRATE) as ser:
            while True:
                if not synchronized:
                    byte = ser.read(1)
                    if byte == b'*':
                        byte = ser.read(2)
                        if byte == b'**':
                            synchronized = True
#                            print("sync'd")
                    else:
                        print("No SYNC: {}".format(byte))
                        send_debug(byte)
                else:
                    byte = ser.read(2+(64*2)+2)	# read possible EOL marker
#                    print(binascii.hexlify(byte))
                    raw = struct.unpack('<64H', byte[2:130])
#                    raw = [(-q12) if (q12 & 0x0800 == 0x0800) else q12 for q12 in raw]
                    raw = [(q12 | 0xf800) if (q12 & 0x0800) else q12 for q12 in raw]
                    temp = np.reshape(np.array(raw)*64/256, (8,8))
#                    print(temp)
                    if not ((byte[-2] == 13) and (byte[-1] == 10)):
                        # We did not read a EOL marker, read more data as we are dealing with TrackMe firmware
#                        print("TrackMe extended data set detected!")
                        byte = byte + ser.read(225+1+2+2 -2)	# not existent EOL marker was part of the first 255 -> read two bytes less
                        pcount = int(byte[-5])
                        person_info = ser.read(pcount*9)
                        byte = byte + person_info
                        if pcount > 0:
                            persons = {x[0]: {'x': x[4]/normalize, 'y': x[3]/normalize } for x in struct.iter_unpack('>BHHHH', person_info)}
                        else:
                            persons = {}

                        msg = {
                            "timestamp": int(time.time()),
                            "count": len(persons),
                            "coords": persons,
                            "rawData": temp.tolist()
                        }
                        if (len(persons) > 0):
                            print(persons)
                        client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))

                    send_debug(b'***'+byte)
                    synchronized = False
    except Exception as e:
        print("Error!")
        print(e)
        client.loop_stop()
        print("Quitting")
        quit = True
#        t.cancel()

    print("done.")
    client.loop_stop()
