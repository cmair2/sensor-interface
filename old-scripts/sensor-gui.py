# -*- coding: utf-8 -*-

import os
import struct
import sys
import time

import PyQt5
import numpy as np
import pyqtgraph as pg
from PyQt5.QtCore import QIODevice, QTimer
from PyQt5.QtNetwork import QTcpSocket, QAbstractSocket
from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.Qt import QtWidgets
from pyqtgraph import Qt
import configparser


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

HOST = config.get('SENSORSERVER',    'HOST', fallback='127.0.0.1')
PORT = config.getint('SENSORSERVER', 'PORT', fallback=10000)

pir = dict()
noise = dict()

sensors = dict()
sensor_rows = dict()


## Reimplements \c pyqtgraph.AxisItem to display time series.
# \code
# from caxistime import CAxisTime
# \# class definition here...
# self.__axisTime=CAxisTime(orientation='bottom')
# self.__plot=self.__glyPlot.addPlot(axisItems={'bottom': self.__axisTime}) # __plot : PlotItem
# \endcode
class CAxisTime(pg.AxisItem):
    ## Formats axis label to human readable time.
    # @param[in] values List of \c time_t.
    # @param[in] scale Not used.
    # @param[in] spacing Not used.
    def tickStrings(self, values, scale, spacing):
        strns = []
        for x in values:
            try:
                strns.append(time.strftime("%H:%M:%S", time.localtime(x)))  # time_t --> time.struct_time
            except ValueError:  # Windows can't handle dates before 1970
                strns.append('')
        return strns


def create_grideye_plot(mac, sensor, name):
    x = []
    y = []

    row = sensor_rows.get(mac, -1)
    if row < 0:
        row = len(sensor_rows.values()) + 1
        sensor_rows[mac] = row

    idx = mac.decode('ascii') + "-" + str(sensor)
    curves = dict()
    plt = win.addPlot(title=name + " (" + idx + ")", row=row, col=sensor)
    curves[name] = dict()
    curves[name]["curve"] = plt.plot(x, y, pen=None, symbol='o', symbolPen=None, symbolSize=10,
                                     symbolBrush=(100, 100, 255, 255))
    plt.enableAutoRange('xy', False)
    plt.setXRange(0, 3548)
    plt.setYRange(0, 3548)
    plt.invertY(True)

    curves["Thermal"] = dict()
    curves["Thermal"]["imageitem"] = pg.ImageItem()
    curves["Thermal"]["imageitem"].setZValue(-1)
    curves["Thermal"]["imageitem"].setOpacity(1)
    curves["Thermal"]["imageitem"].scale(3548 / 8, 3548 / 8)
    curves["Thermal"]["imageitem"].setLevels([25, 31])
    plt.addItem(curves["Thermal"]["imageitem"])
    return plt, curves


def create_scrolling_plot(mac, sensor, items):
    idx = mac.decode('ascii') + "-" + str(sensor)
    row = sensor_rows.get(mac, -1)
    if row < 0:
        row = len(sensor_rows.values()) + 1
        sensor_rows[mac] = row

    date_axis = CAxisTime(orientation='bottom')

    curves = dict()
    plt = win.addPlot(title=items[0][0] + " (" + idx + ")", row=row, col=sensor, axisItems={'bottom': date_axis})
    plt.enableAutoRange('xy', True)
    for (name, x, y, color) in items:
        curve = plt.plot(x, y, pen=pg.mkPen(color))
        curves[name] = dict()
        curves[name]["curve"] = curve
        curves[name]["x"] = x
        curves[name]["y"] = y
    return plt, curves


def scroll_curve(curve, newx, newy, history):
    min_time = curve["x"][0]
    max_time = curve["x"][-1]
    if (max_time-min_time) < history:
        curve["x"] = np.append(curve["x"], newx)
        curve["y"] = np.append(curve["y"], newy)
    else:
        new_min_time_pos = np.where(curve["x"] > (max_time - history))[0][0]
        curve["y"] = curve["y"][new_min_time_pos:]
        curve["y"][-1] = newy
        curve["x"] = curve["x"][new_min_time_pos:]
        curve["x"][-1] = newx
    curve["curve"].setData(curve["x"], curve["y"])


class Client(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.tcpSocket = QTcpSocket(self)
        self.blockSize = 0
        self.tcpSocket.readyRead.connect(self.dealCommunication)
        self.tcpSocket.error.connect(self.displayError)
        self.tcpSocket.disconnected.connect(self.disconnected)
        print("Connecting...")
        self.makeRequest()

    def disconnected(self):
        if self.tcpSocket.state() == QAbstractSocket.UnconnectedState:
            print("Disconnected: Reconnecting...")
            self.makeRequest()
        else:
            print("WTF?")
            print(self.tcpSocket.state())

    def makeRequest(self):
        self.tcpSocket.abort()
        self.tcpSocket.connectToHost(HOST, PORT, QIODevice.ReadWrite)
        self.tcpSocket.waitForConnected(2000)

    def dealCommunication(self):
        history = 60*60*2
        while (self.tcpSocket.bytesAvailable() > (6 + 17 + 8 + 1 + 1)):
            bytes_avail = self.tcpSocket.bytesAvailable()
            data = self.tcpSocket.read(6 + 17 + 8 + 1 + 1)
            #            print(data)
#            print(len(data))
            manufacturer, mac, timestamp, msg, length = struct.unpack("<6s17sQBB", data)
            timestamp = timestamp / 1000
#            print("M: " + str(manufacturer) + ", B: " + str(mac) + ", G: " + str(msg))
#            length = int.from_bytes(self.tcpSocket.read(1), byteorder='little')
#            print(str(manufacturer) + ", " + str(mac) + ", " + str(msg) + ": " + str(bytes_avail) + "/" + str(length))
            while (self.tcpSocket.bytesAvailable() < length):
                print("Waiting for more data: " + str(self.tcpSocket.bytesAvailable()) + "/" + str(length))
                time.sleep(0.1)
            data = self.tcpSocket.read(length)
            if len(data) != length:
                print(str(len(data)) + "//" + str(length) + ": " + data)
            if msg == 0x11:
                objects = len(data)/2
                coords = struct.unpack("{:d}H".format(int(objects)), data)
                x = list(coords[0::2])
                y = list(coords[1::2])
                idx = mac.decode('ascii') + "-" + str(msg)
                plot, curves = sensors.get(idx, (None, None))
                if plot is None:
                    plot, curves = create_grideye_plot(mac, msg, "Presence")
                curves["Presence"]["curve"].setData(x=x, y=y)
                sensors[idx] = (plot, curves)
            elif msg == 0x12:
                    temp = struct.unpack("{:d}H".format(int(len(data) / 2)), data)
                    idx = mac.decode('ascii') + "-" + str(msg-1)
                    plot, curves = sensors.get(idx, (None, None))
                    if plot is None:
                        plot, curves = create_grideye_plot(mac, msg-1, "Presence")
                    d = np.array(temp).reshape((8,8)).T / 256
                    curves["Thermal"]["imageitem"].setImage(d)
                    sensors[idx] = (plot, curves)
            elif msg == 0x21:
                event = struct.unpack("<B", data)[0]
                idx = mac.decode('ascii') + "-" + str(msg)
                plot, curves = sensors.get(idx, (None, None))
                if plot is None:
                    plot, curves = create_scrolling_plot(mac, msg,
                                                         [("PIR",
                                                           np.arange(timestamp - 120, timestamp, 1.0),
                                                           np.zeros(1),
                                                           'w'),
                                                          ])
                    sensors[idx] = (plot, curves)
                scroll_curve(curves["PIR"], timestamp-0.01, curves["PIR"]["y"][-1], history)
                scroll_curve(curves["PIR"], timestamp, event, history)
            elif msg == 0x31:
                if len(data) != 2:
                    print("Len != 2!")
                    print(len(data))
                    print(data)
                n = struct.unpack("<H", data)[0]
                n = 20 * np.log10((126.19147441253433656304 * n / 1000000.0) / (2.0 * 0.00001))
                idx = mac.decode('ascii') + "-" + str(msg)
                (plot, curves) = sensors.get(idx, (None, None))
                if plot is None:
                    plot, curves = create_scrolling_plot(mac, msg,
                                                         [("Noise",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, n, np.float),
                                                           'w'),
                                                          ])
                    sensors[idx] = (plot, curves)
                scroll_curve(curves["Noise"], timestamp, n, history)
            elif msg == 0x41:
                co2, status, resistance, voc = struct.unpack("<HBIH", data)
                idx = mac.decode('ascii') + "-" + str(msg)
                (plot, curves) = sensors.get(idx, (None, None))
                if plot is None:
                    plot, curves = create_scrolling_plot(mac, msg,
                                                         [("VOC",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, voc, np.int32),
                                                           'g'),
                                                          ("CO2",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, co2, np.int32),
                                                           'y')
                                                          ])
                    sensors[idx] = (plot, curves)
                scroll_curve(curves["VOC"], timestamp, voc, history)
                scroll_curve(curves["CO2"], timestamp, co2, history)
            elif msg == 0x51:
                clear, red, green, blue = struct.unpack("<HHHH", data)
                idx = mac.decode('ascii') + "-" + str(msg)
                (plot, curves) = sensors.get(idx, (None, None))
                if plot is None:
                    history = 5*60
                    plot, curves = create_scrolling_plot(mac, msg,
                                                         [("Clear",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, clear, np.int32),
                                                           'w'),
                                                          ("Red",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, red, np.int32),
                                                           'r'),
                                                          ("Green",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, green, np.int32),
                                                           'g'),
                                                          ("Blue",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, blue, np.int32),
                                                           'b')
                                                          ])
                    sensors[idx] = (plot, curves)
                scroll_curve(curves["Clear"], timestamp, clear, history)
                scroll_curve(curves["Red"], timestamp, red, history)
                scroll_curve(curves["Green"], timestamp, green, history)
                scroll_curve(curves["Blue"], timestamp, blue, history)
            elif msg == 0x61:
                iaq, temp, humidity, pressure = struct.unpack("<ffff", data)
                pressure = pressure/1000
                idx = mac.decode('ascii') + "-" + str(msg)
                (plot, curves) = sensors.get(idx, (None, None))
                if plot is None:
                    plot, curves = create_scrolling_plot(mac, msg,
                                                         [("Air Quality",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, iaq, np.int32),
                                                           'w'),
                                                          ("Temperature",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, temp, np.int32),
                                                           'r'),
                                                          ("Humidity",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, humidity, np.int32),
                                                           'g'),
                                                          ("Pressure",
                                                           np.full(1, timestamp, np.double),
                                                           np.full(1, pressure, np.int32),
                                                           'b')
                                                          ])
                    sensors[idx] = (plot, curves)
                scroll_curve(curves["Air Quality"], timestamp, iaq, history)
                scroll_curve(curves["Temperature"], timestamp, temp, history)
                scroll_curve(curves["Humidity"], timestamp, humidity, history)
                scroll_curve(curves["Pressure"], timestamp, pressure, history)
            else:
                # Unknown message: throw away
                # data = self.tcpSocket.read(length)
                print("Unknown msg: " + str(msg) + ":" + str(length) + ": " + str(data))

    def displayError(self, socketError):
#        print(self, "The following error occurred: %s." % self.tcpSocket.errorString())
        if socketError == QAbstractSocket.RemoteHostClosedError:
            self.tcpSocket.disconnectFromHost()
        # if socketError == QAbstractSocket.ConnectionRefusedError:
        #        else:
        if self.tcpSocket.state() == QAbstractSocket.UnconnectedState:
            QTimer.singleShot(1000, self.makeRequest)
#            print("Error: Reconnecting...")
#            self.makeRequest()


# QtGui.QApplication.setGraphicsSystem('raster')
app = QtGui.QApplication([])
# mw = QtGui.QMainWindow()
# mw.resize(800,800)

win = pg.GraphicsWindow(title="Sensorboard")
win.resize(1920, 1200)
win.showFullScreen()
win.setWindowState(QtCore.Qt.WindowMaximized)
win.setWindowTitle('Sensorboard')

# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)


sys._excepthook = sys.excepthook


def exception_hook(exctype, value, traceback):
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)


sys.excepthook = exception_hook

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys

    client = Client()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
