#!/usr/bin/env python

import json
import uuid
import websocket
from threading import Thread
import signal
import sys
import os
import configparser
import time
import paho.mqtt.client as mqtt
from threading import Timer
import datetime


config = configparser.ConfigParser()
filename = os.path.dirname(os.path.realpath(__file__)) + "/config.cfg"
config.read(filename)

HOST = config.get('VAYYAR',    'HOST', fallback='10.42.0.68')
PORT = config.getint('VAYYAR', 'PORT', fallback=1234)
TYPE = config.get('VAYYAR',    'TYPE', fallback='peoplecounter_absolute')

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')


wsurl = "ws://{}:{}".format(HOST, PORT)

UUID = config.get('VAYYAR',    'UUID', fallback=None)
if not UUID:
    UUID = uuid.uuid4()
    print(UUID)
    print(str(UUID))
    config.set('VAYYAR', 'UUID', str(UUID))
    config.write(open(filename, 'w'))

mqtt_topic = 'sensor/' + TYPE + '/' + UUID

persons = None
quit = False
dimensions = {}
t = None
timeoutcnt = 0

def timeout():
    global persons
    print(persons)
    global timeoutcnt
    if (persons is not None) or (timeoutcnt > 5):
        persons = None
        timeoutcnt = 0
        request_data(ws)
    timeoutcnt = timeoutcnt + 1

#    msg = {"timestamp": time.time(), "count": len(persons), "coords": persons}
#    client.publish(mqtt_topic, payload=json.dumps(msg).encode('UTF-8'))
#    print(msg)
    if not quit:
        t = Timer(0.25, timeout)
        t.start()


client = mqtt.Client()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected to MQTT server {} with result code {}.".format(MQTTHOST, rc))

def on_mqtt_message(client, userdata, msg):
    print("Got: " + msg.topic+" "+str(msg.payload))


def start_sensor(ws):
    ws.send(json.dumps({"Type": "COMMAND",
                        "ID": "START",
                        "Payload": {
                            "outputs": ['rawImage_XY', 'LocationMatrix', 'PostureVector', 'NumOfPeople', 'PeopleCounterXY'],
                         }}).encode("ascii"))

def stop_sensor(ws):
    ws.send(json.dumps({"Type": "COMMAND",
                        "ID": "STOP",
                         }).encode("ascii"))

def request_data(ws):
    ws.send(json.dumps({"Type": "QUERY", "ID": "data" }).encode("ascii"))




def on_message(ws, message):
    global persons
    global dimensions
    global t
    m = json.loads(message)
#    if not m["ID"] == "data":
#    with open("vayyar-log.log", "a") as f:
#        f.write("{}: {}\n".format(datetime.datetime.now(), m["ID"]))
#    print("message: " + message)
    if m['ID'] == "SET_PARAMS":
        dimensions = dict(zip(["xmin", "xmax", "ymin", "ymax", "zmin", "zmax"], m['Payload']['Cfg.MonitoredRoomDims']))
        start_sensor(ws)

    if m['ID'] == "GET_STATUS":
        if m['Payload']['status'] == "IMAGING":
            t = Timer(10, timeout)
            t.start()
        if m['Payload']['status'] == "WAITING_FOR_START":
            start_sensor(ws)
        if m['Payload']['status'] == "ERROR":
            print("ERROR: {}".format(m['Payload']['errorMessage']))
    if m["ID"] == "data":
        if len(dimensions) <= 0:
#            with open("vayyar-log.log", "a") as f:
#                f.write("dimensions is not defined. quitting: {}: {}\n".format(datetime.datetime.now(), m["ID"]))
            signal_handler(None, None) # Call signal handler. It will stop the sensor and quit.
            return
        msg = {"timestamp": time.time(),
               "vendor": "Vayyar",
               "model": "vCUBE",
               "count": int(m['Payload']['NumOfPeople']),
               "coords": {c: {"x": (x-dimensions['xmin'])/(dimensions['xmax']-dimensions['xmin']),
                              "y": (y-dimensions['ymin'])/(dimensions['ymax']-dimensions['ymin'])
                             } for c, (x, y, z) in enumerate(m['Payload']['LocationMatrix']) if x != "NaN" and y != "NaN"
                         },
               "postures": dict((c, x.lower()) for c, x in enumerate(m['Payload']['PostureVector']) if c < int(m['Payload']['NumOfPeople'])),
               "dimensions": dimensions,
               "rawData": m['Payload']['rawImage_XY'],
              }
        persons = msg['coords']
#        if len(msg['coords']) > 0:
        client.publish(mqtt_topic, payload=json.dumps(msg).encode('ascii'))
#        print(msg['coords'], msg['postures'])

#        print(len(msg['rawData'][0]))
#        print(len(msg['rawData']))


ws = None




def on_error(ws, error):
    print("Websocket Error: " + str(error))
#    with open("vayyar.log", "a") as f:
#        f.write("WEBSOCKET-ERROR: {}\n".format(datetime.datetime.now()))
    try:
        ws.close()
    except Exception as e:
        print(e)
    connect_to_vayyar()

def on_close(ws):
    print("### closed ###")
    client.loop_stop()

def on_open(ws):
    print("### open ###")
    persons = {}
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)

    client.loop_start()


    defaultDisplayParams = {
        'Cfg.MonitoredRoomDims': [-2, 2, -2, 2, 0.7, 1.8],
        'Cfg.Common.sensorOrientation.mountPlane': 'xy',
        'Cfg.Common.sensorOrientation.transVec': [0.0, 0.0, 2.7],
        'Cfg.imgProcessing.substractionMode': 6.0,
        'Cfg.TargetProperties.MaxPersonsInArena': 10.0,
        'Cfg.TargetProperties.StandingMaxHeight': 2.0,
        'Cfg.TargetProperties.StandingMinHeight': 1.55,
        'Cfg.TargetProperties.SittingMinHeight': 0.8,
        'Cfg.TargetProperties.LyingMinHeight': 0.2,
        'Cfg.TargetProperties.PersonRadius': 0.5,
        'MPR.save_dir': '',
        'MPR.read_from_file': 0.0,
        'MPR.save_to_file': 0.0,
        'MPR.save_image_to_file': 0.0,
        'Cfg.OutputData.save_to_file': 0.0,
        'Cfg.ExternalGUI.FilterImage.TH': 0.0,
        'Cfg.ExternalGUI.FilterImage.numOfSd': 3.0,
        'Cfg.PeopleCounter.inCarIsLocked': False,
        'Cfg.Zones.Beds': None
    }

    print("SET_PARAMS")
    ws.send(json.dumps({"Type": "COMMAND",
             "ID": "SET_PARAMS",
             "Payload": defaultDisplayParams
         }).encode("ascii"))

    ws.send(json.dumps({"Type": "QUERY", 'ID': "data"}))


ws = None

def connect_to_vayyar():
#    with open("vayyar.log", "a") as f:
#        f.write("Connecting: {}\n".format(datetime.datetime.now()))
    print("Connecting to {}".format(wsurl))
    ws = websocket.WebSocketApp(wsurl,
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    return ws


def signal_handler(signal, frame):
        def run(*args):
            global quit
            print('\nStopping sensor')
            quit = True
            stop_sensor(ws)
            ws.close()
            client.loop_stop()

        Thread(target=run).start()


if __name__ == "__main__":
#    with open("vayyar.log", "a") as f:
#        f.write("Start: {}\n".format(datetime.datetime.now()))
#    t = Timer(3, timeout)
#    t.start()
    ws = connect_to_vayyar()
    signal.signal(signal.SIGINT, signal_handler)
    ws.run_forever()
    quit = True
#    stop_sensor(ws)
#    ws.close()
    msg = {
        "vendor": "Vayyar",
        "model": "vCUBE",
        "timestamp": time.time(),
        "count": 0,
        "coords": {}}
    client.publish(mqtt_topic, payload=json.dumps(msg).encode('ascii'))
