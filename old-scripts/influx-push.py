#!/usr/bin/python3

import json
import numpy as np
import socket
import struct
import datetime
import pytz
from influxdb import InfluxDBClient

HOST = '127.0.0.1'
PORT = 10000

stop = False

HEADER_LENGTH = 6+17+8+1+1


db_host = 'osramclouddemo.westeurope.cloudapp.azure.com'
db_port = '8086'
dbname = 'sensordata'
dbuser = 'sensorboard'
dbuser_password = 'SensorB0red'
query = 'select value from temperature;'
json_body = [
    {
        "measurement": "temperature",
        "tags": {
            "board": "aa:bb:cc:dd:ee:ff",
        },
        "time": datetime.datetime.utcnow().isoformat("T")+"Z",
        "fields": {
            "Float_value": 0.64,
        }
    }
]


client = InfluxDBClient(db_host, db_port, dbuser, dbuser_password, dbname)

#print("Write points: {0}".format(json_body))
#client.write_points(json_body)

#print("Querying data: " + query)
#result = client.query(query)

#print("Result: {0}".format(result))


while True:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print("Connecting to %s:%s..." % (HOST, PORT))
        s.connect((HOST, PORT))
        data = b''
        while not stop:
            d = s.recv(4096)
            if len(d) > 3000:
                print("Congestion warning while saving data!")
#            print("Got %d bytes" % (len(d)), end='')
            if d == '':
                stop = True
                continue
            data = data + d
#            print(b'read: ' + data)
            while len(data) >= (HEADER_LENGTH):
#                print(data[:HEADER_LENGTH])
                header, mac, timestamp, msg, length = struct.unpack("<6s17sQBB", data[:HEADER_LENGTH])
#                print(header, mac, timestamp, msg, length)
                timestamp = timestamp / 1000
                data = data[HEADER_LENGTH:]
#                print(b'data: ' + data)

                if len(data) >= length:
                    message = {
                        "time": datetime.datetime.fromtimestamp(timestamp, tz=pytz.utc).isoformat("T"),
                        "tags": {"source": "sensorboard", "board": mac.decode('ascii') },
#                        'Ambient': { 'VOC': None, 'iaq': None, 'temperature': None, 'humidity': None, 'pressure': None },
#                        'Light': { 'clear': None, 'red': None, 'green': None, 'blue': None },
                    }
                    if msg == 0x11:
                        coords = struct.unpack("<{:d}H".format(int(length/2)), data[:length])
                        coo = [coords[i:i+2] for i in range(0, len(coords), 2)]
#                        message['Presence'] = coo
#                        client.publish("sensor/"+mac.decode('ascii'), payload=json.dumps(message).encode('UTF-8'))

                    elif msg == 0x12:
                        print("Ignoring thermal image")
                    elif msg == 0x21:
                        event = struct.unpack("<B", data[:length])[0]
                    elif msg == 0x31:
                        n = struct.unpack("<H", data[:length])[0]
                        n = 20 * np.log10((126.19147441253433656304 * n / 1000000.0) / (2.0 * 0.00001))
                    elif msg == 0x41:
                        co2, status, resistance, voc = struct.unpack("<HBIH", data[:length])
#                        message['Ambient'] = {
#                            'VOC': voc,
#                            'CO2': co2
#                        }
#                        client.publish("sensor/"+mac.decode('ascii'), payload=json.dumps(message).encode('UTF-8'))
                    elif msg == 0x51:
                        clear, red, green, blue = struct.unpack("<HHHH", data[:length])
#                        message['Light'] = {
#                            'clear' : round(clear, 2),
#                            'red': round(red, 2),
#                            'green': round(green, 2),
#                            'blue': round(blue, 2)
#                        }
#                        client.publish("sensor/"+mac.decode('ascii'), payload=json.dumps(message).encode('UTF-8'))
                    elif msg == 0x61:
                        iaq, temperature, humidity, pressure = struct.unpack("<ffff", data[:length])
                        message["fields"] = {
                            "iaq" : round(iaq, 2),
                            "temperature": round(temperature, 2),
                            "humidity": round(humidity, 2),
                            "pressure": round(pressure/100, 2)
                        }
                        message["measurement"] = "ambient air"
                        print(message)
                        client.write_points([message])

                    data = data[length:]
#                    print(b'data2: ' + 	data)
#            print(".")
    print("Wait...")
    time.sleep(1)

client.disconnect()
client.loop_stop()
