#!/usr/bin/env python

import json
import websocket
from threading import Timer

# wsurl = "wss://wsx.irlynx.com/?uuid=7f2f60e6a0ad542"
wsurl = "wss://wsx.irlynx.com/?uuid=1a6efb16fd28e750"

import websocket
import threading
import time
import paho.mqtt.client as mqtt

MQTTHOST = 'm23.cloudmqtt.com'      # The remote host
MQTTPORT = 16856                    # The same port as used by the server
USERNAME = "testuser"
PASSWORD = "test"

persons = {}
client = mqtt.Client()


def timeout():
    msg = {"timestamp": time.time(), "count": len(persons)}
    client.publish("sensor/irlynx", payload=json.dumps(msg).encode('UTF-8'))
#    print(msg)
    t = Timer(3, timeout)
    t.start()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    t = Timer(3, timeout)
    t.start()
#    client.subscribe("sensor/irlynx")

def on_mqtt_message(client, userdata, msg):
    print("Got: " + msg.topic+" "+str(msg.payload))


def on_message(ws, message):
    global persons
#    print("message: " + message)
    message = message.split('},')
    message[-1] = message[-1].strip('}')
    for m in message:
        m = m + "}"
#        print(m)
        try:
            s = json.loads(m)
        except Exception as e:
            print("ERROR: ignoring message: " + str(m) + ": " + str(e))
            return
        if 'D' in s:
            numbers = [int(x) for x in s['D'].split(' ')]
            if numbers[0] == 34:
                msg = {"timestamp": time.time(), "count": numbers[2]}
                client.publish("sensor/irlynx", payload=json.dumps(msg).encode('UTF-8'))
                if (numbers[2] != len(persons)):
                    persons = {}
#                print("34: " + str(msg))
            elif numbers[0] == 51:
                pid = numbers[2]
                persons[pid] = {numbers[4], numbers[5]}
#                msg = {"timestamp": int(time.time()), "count": len(persons)}
#                client.publish("sensor/irlynx", payload=json.dumps(msg).encode('UTF-8'))
#                print(msg)
            elif numbers[0] == 52 or numbers[0] == 53:
                pid = numbers[2]
                if pid in persons:
                    del persons[pid]
                msg = {"timestamp": int(time.time()), "count": len(persons)}
                client.publish("sensor/irlynx", payload=json.dumps(msg).encode('UTF-8'))
#                print("52 " + str(msg))


ws = None




def on_error(ws, error):
    print("E: " + str(error))
    try:
        ws.close()
    except Exception as e:
        print(e)
#    ws = connect_to_irlynx()

def on_close(ws):
    print("### closed ###")
    ws = connect_to_irlynx()

def on_open(ws):
    print("### open ###")
    persons = {}
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)

    client.loop_start()

def connect_to_irlynx():
    print("Trying to connect...")
    ws = websocket.WebSocketApp(wsurl,
                              subprotocols=["lws-mirror-protocol"],
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    return ws


if __name__ == "__main__":
    websocket.enableTrace(True)

    while(True):
        ws = connect_to_irlynx()

        ws.run_forever()
        ws.close()
        client.loop_stop()
