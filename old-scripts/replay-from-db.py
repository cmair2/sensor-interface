#!/usr/bin/python3

import datetime
import numpy as np
import psycopg2
import socket
import struct
import time

HOST = 'localhost'      # The remote host
PORT = 10000            # The same port as used by the server
DATABASE = "sensordata"
USER = "sensorhub"
PASSWORD = "RaspberrySensors"

SQL = "SELECT * FROM sensordata WHERE mac = %s ORDER BY ts"

print("Connecting to database " + USER + "@" + HOST)
conn = psycopg2.connect(host=HOST, database=DATABASE, user=USER, password=PASSWORD)

c = conn.cursor()

#t = ('RHAT',)
#c.execute('SELECT * FROM data WHERE symbol=?', t)

stop = False

HEADER_LENGTH = 6+17+8+1+1





sockets = []

sock = socket.socket()
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('0.0.0.0', 10001))

while True:
    print("Listening..")
    sock.listen(100)
    sock.setblocking(True)
    conn, addr = sock.accept()
    print("Accepted: " + str(conn))
    sockets.append(conn)


    c.execute(SQL, ("d6:35:97:55:ac:77",))
    item = c.fetchone()
    while item:
        next_item = None
        print(item)
        binmsg = struct.pack("<6s17sQ", "\nOSRAM".encode('ascii'), item[0].encode('ascii'), int(item[1].timestamp()*1000))
#    print(binmsg)
        raw = None
        if item[2] == 0x11:
             coords = []
             coords.append(item[3] >> 16)
             coords.append(item[3] & 0xFF)
#             print(coords)
             next_item = c.fetchone()
#             print(next_item)
             while next_item and (next_item[1] == item[1]) and (next_item[0] == item[0]) and (next_item[2] == item[2]):
                 coords.append(next_item[3] >> 16)
                 coords.append(next_item[3] & 0xFF)
#                 print(coords)
                 next_item = c.fetchone()
#                 print(next_item)

             length = len(coords)
             raw = struct.pack("<BB{:d}H".format(length), 0x11, length*2, *coords)
#            length = len(s['Presence'])
#            coords = [item for sublist in s['Presence'] for item in sublist]
#            raw = struct.pack("<BB{:d}H".format(length*2), 0x11, length*4, *coords)
#            raw = struct.pack("<BBHHHH", 0x51, 8, s['Light']['clear'], s['Light']['red'], s['Light']['green'], s['Light']['blue'])
        elif item[2] == 0x21:
            raw = struct.pack("<BBB", 0x21, 1, item[3])
        elif item[2] == 0x31:
            raw = struct.pack("<BBH", 0x31, 2, item[3])
        elif item[2] == 0x41:
            raw = struct.pack("<BBHBIH", 0x41, 9, 0, 0, 0, item[3])
        elif (item[2] & 0xF0) == 0x50:
             values = [item[3]]
             next_item = c.fetchone()
#             print(next_item)
             while (next_item is not None) and (next_item[1] == item[1]) and (next_item[0] == item[0]) and ((next_item[2] & 0xF0) == (item[2] & 0xF0)):
                 values.append(next_item[3])
                 next_item = c.fetchone()
#                 print(next_item)

             if len(values) >= 4:
                 raw = struct.pack("<BBHHHH", 0x51, 8, values[0], values[1], values[2], values[3])

        if (raw is not None):
#            print(sockets)
            msg = binmsg + raw
            print(msg)
            for s in sockets:
                try:
                    s.send(msg)
                except Exception as e:
                    print(e)
                    sockets.remove(s)
#                    s.close()
            raw = None


        if next_item is None:
            next_item = c.fetchone()
        if next_item:
            time.sleep((next_item[1] - item[1]).microseconds/10000000)
        item = next_item


for s in sockets:
    s.close()

sock.close()

print("Quit.")
