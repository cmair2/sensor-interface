import os
import time
import json
import random
import paho.mqtt.client as mqtt
from threading import Timer
import configparser
from datetime import datetime, timedelta


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

MQTTHOST = config.get('MQTT',    'HOST',     fallback='localhost')
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=1883)
USERNAME = config.get('MQTT',    'USERNAME', fallback='')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='')

OLT_MQTTHOST = config.get('LIGHTELLIGENCE', 'HOST', fallback="mqtt.preview.oltd.de")
OLT_MQTTPORT = config.get('LIGHTELLIGENCE', 'PORT', fallback=8883)

stop = False

olt_client = mqtt.Client('rpi-gateway_%d' % (random.randint(0, 1024)))
olt_client.tls_set(ca_certs='chain.pem', certfile='device_cert.pem', keyfile='device_key.pem')

def on_olt_mqtt_log(client, userdata, level, buf):
    print(client, userdata, level, buf)
    pass

def on_olt_mqtt_connect(client, userdata, flags, rc):
    print("Connected to OLT MQTT server {} with result code {} ({}).".format(OLT_MQTTHOST, mqtt.connack_string(rc), rc))

def on_olt_mqtt_disconnect(client, userdata, rc):
    print("Disconnected from OLT with code {} ({})".format(mqtt.connack_string(rc), rc))

def on_olt_mqtt_message(client, userdata, msg):
    print("Got from OLT: " + msg.topic+" "+str(msg.payload))



client = mqtt.Client()
last_message_sent_timestamp = {}

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("sensor/#")

def on_message(client, userdata, msg):
#    print(msg.topic)
    if "transform" in msg.topic:
        return
    s = json.loads(msg.payload.decode('UTF-8'))
#    print(s['coords'])
#    print([s['coords'][i] for i in s['coords']])

    payload = {}
    if 'peoplecounter_absolute' in msg.topic:
        coords = [s['coords'][i] for i in s['coords']]
        payload = {
            "type": "attributes",
            "value": {
                "count": len(coords),
                "coordinates": coords
            }
        }
        if msg.topic.endswith('irlynx'):
            payload["deviceId"] = 'cbcbc56c-8add-4673-ae27-e77f9f786f3f'
        elif msg.topic.endswith('vayyar'):
            payload["deviceId"] = '1c14f068-d89c-4d8f-b0fc-1c33ed292a01'
        elif msg.topic.endswith('camera'):
            print('Camera!\n')
            payload["deviceId"] = 'b6fe7144-5734-4d97-b6b2-c74f26aab5e0'
#        elif msg.topic.endswith('grideyekompakt'):
#            payload['deviceId'] = '52a431f8-dbd1-4f1f-98ae-7d2d71a83c02'

    elif 'ambientair' in msg.topic:
        if 'ruuvitag/D9:75:77:70:36:71' in msg.topic:
            payload = {
                'deviceId': 'af10df4e-20b2-4a11-95a9-c9e396015cf9',
                'type': 'attributes',
                'value': s,
            }

    default_timedelta = timedelta(seconds=2)
    if "deviceId" in payload and \
            (last_message_sent_timestamp.get(msg.topic, datetime.now() - default_timedelta) + \
                timedelta(seconds=1)) < datetime.now():
        last_message_sent_timestamp[msg.topic] = datetime.now()
        print(payload)
        olt_client.publish("data-ingest", payload=json.dumps(payload).encode('UTF-8'))
#        push_data_ruuvitag()



olt_client.on_connect = on_olt_mqtt_connect
olt_client.on_message = on_olt_mqtt_message
olt_client.on_disconnect = on_olt_mqtt_disconnect
olt_client.on_log = on_olt_mqtt_log
olt_client.connect_async(OLT_MQTTHOST, OLT_MQTTPORT)
olt_client.loop_start()

client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(USERNAME, PASSWORD)
client.connect_async(MQTTHOST, MQTTPORT)
client.loop_start()

while not stop:
    time.sleep(1)

client.loop_stop()
olt_client.loop_stop()
