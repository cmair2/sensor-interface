''' Present an interactive function explorer with slider widgets.

Scrub the sliders to change the properties of the ``sin`` curve, or
type into the title text box to update the title of the plot.

Use the ``bokeh serve`` command to run the example by executing:

    bokeh serve sliders.py

at your command prompt. Then navigate to the URL

    http://localhost:5006/sliders

in your browser.

'''
import numpy as np

from bokeh.io import curdoc
from bokeh.layouts import row, column, widgetbox
from bokeh.models import ColumnDataSource, BoxAnnotation, HoverTool
from bokeh.models.widgets import Slider, TextInput
from bokeh.models.formatters import DatetimeTickFormatter
from bokeh.plotting import figure
#from bokeh.charts import TimeSeries

import psycopg2
import pandas as pd


conn = psycopg2.connect(host="localhost", database="sensordata", user="sensorhub", password="RaspberrySensors")

query_sensors = """
SELECT DISTINCT mac, type
FROM sensordata
WHERE mac = 'd6:35:97:55:ac:77'
ORDER BY type
"""

query_value = """
SELECT floor(avg(extract(epoch from ts))) as ts, avg(value) as value
FROM sensordata
WHERE mac = '{}' and type = {} group by floor(extract(epoch from ts)/({}))
ORDER BY ts
"""

sensortype = dict()
sensortype[0x10] = "Presence"
sensortype[0x20] = "PIR"
sensortype[0x30] = "Noise"
sensortype[0x40] = "VOC"
sensortype[0x50] = "Light"
sensortype[0x60] = "Ambient"

typecolors = dict()
typecolors[0x11] = 'black'	# Presence coordinates
typecolors[0x12] = 'black'	# Raw IR image
typecolors[0x21] = 'black'	# PIR
typecolors[0x31] = 'black'	# Noise
typecolors[0x41] = 'green'	# VOC
typecolors[0x51] = 'black'	# Colors
typecolors[0x52] = 'red'
typecolors[0x53] = 'green'
typecolors[0x54] = 'blue'
typecolors[0x61] = 'black'	# AIQ
typecolors[0x62] = 'red'	# Temperature
typecolors[0x63] = 'green'	# Humidity
typecolors[0x64] = 'blue'	# Pressure


linestyle = dict()
linestyle['d2:35:c9:ab:f6:17'] = "solid"
linestyle['d6:35:97:55:ac:77'] = "4 4"

division_factor = 60

graphs_df = pd.read_sql(query_sensors, conn)

print(graphs_df)

data = dict()
plots = dict()
oldtype = None
plot = None

for index, sensor in graphs_df.iterrows():
    print("Querying database for {} {} (index {})".format(sensor.mac, sensor.type, index))
    df = None
    if sensor.type != 0x10:
        df = pd.read_sql(query_value.format(sensor.mac, sensor.type, division_factor), conn, parse_dates={'ts': {'unit': 's'}})
    else:
        df = pd.read_sql(query_value.format(sensor.mac, sensor.type, division_factor), conn, parse_dates={'ts': {'unit': 's'}})

    data[index] = ColumnDataSource(data=dict(x=df['ts'], y=df['value']))

    print("Type: 0x{:x}".format(sensor.type))
    if oldtype != sensor.type >> 4:
        oldtype = sensor.type >> 4
        if (len(plots) > 0):
            plot = figure(title="{}".format(sensortype[sensor.type & 0xF0]), output_backend="webgl", tools="crosshair,pan,reset,save,wheel_zoom,box_zoom", width=1500, x_range=plots[0].x_range)
        else:
            plot = figure(title="{}".format(sensortype[sensor.type & 0xF0]), output_backend="webgl", tools="crosshair,pan,reset,save,wheel_zoom,box_zoom", width=1500, x_axis_type="datetime")

        if (sensor.type in ['AIQ', 'CO2']):
            plot.add_layout(BoxAnnotation(top=1000, fill_alpha=0.1, fill_color='green'))
            plot.add_layout(BoxAnnotation(bottom=1000, top=1500, fill_alpha=0.1, fill_color='yellow'))
            plot.add_layout(BoxAnnotation(bottom=1500, fill_alpha=0.1, fill_color='red'))

        plots[index] = plot

    plot.line('x', 'y', source=data[index], line_color=typecolors[sensor.type], legend=sensor.mac)
#    plot.circle('x', 'y', source=data[index], line_color=typecolors[sensor.type])
    plot.legend.location = "bottom_left"
    plot.xaxis[0].formatter = DatetimeTickFormatter()
#    plot.add_tools(HoverTool(tooltips=[
#        ("Date", "@x"),
#        ("Value", "@y{0.0}")
#    ], mode='vline'))


#print(df)

# Set up plot

#plot.line('timestamp', 'value', source=df, line_width=3, line_alpha=0.6)


# Set up widgets
text = TextInput(title="Resolution", value=str(division_factor))
#offset = Slider(title="offset", value=0.0, start=-5.0, end=5.0, step=0.1)
#amplitude = Slider(title="amplitude", value=1.0, start=-5.0, end=5.0)
#phase = Slider(title="phase", value=0.0, start=0.0, end=2*np.pi)
#freq = Slider(title="frequency", value=1.0, start=0.1, end=5.1)

inputs = widgetbox(text)



# Set up callbacks
def update_resolution(attrname, old, new):
    print(attrname)
    print(old)
    print(new)
    division_factor = int(new)
    if division_factor < 1:
        division_factor = 1


#    plots = []

#    graphs_df = pd.read_sql(query_sensors, conn)
    for index, sensor in graphs_df.iterrows():
        print("Querying database for {} {}".format(sensor.mac, sensor.type))
        df = pd.read_sql(query_value.format(sensor.mac, sensor.type, division_factor), conn, parse_dates={'ts': {'unit': 's'}})
        data[index].data = dict(x=df['ts'], y=df['value'])

        print("Done.")


text.on_change('value', update_resolution)

#for w in [offset, amplitude, phase, freq]:
#    w.on_change('value', update_data)


# Set up layouts and add to document

graphs = [v for v in plots.values()]

graphs.append(inputs)

curdoc().add_root(column(graphs))
curdoc().title = "Environmental Monitor"
