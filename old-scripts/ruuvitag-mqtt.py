#!/usr/local/bin/python3

import os
import json
import paho.mqtt.client as mqtt
import configparser
from ruuvitag_sensor.ruuvi import RuuviTagSensor
from threading import Timer


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')

sensor_topic = "sensor/ambientair/ruuvitag/"
SCRIPT_RESTART_TIMEOUT = 20


client = mqtt.Client()
t = Timer(SCRIPT_RESTART_TIMEOUT, quit)
t.start()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    pass

def on_mqtt_message(client, userdata, msg):
    pass


def handle_data(found_data):
    global t
#    print('MAC ' + found_data[0])
#    print(found_data[1])
    t.cancel()
    t = Timer(SCRIPT_RESTART_TIMEOUT, quit)
    t.start()
    msg = {
        'acceleration': {
                'x': found_data[1]['acceleration_x'],
                'y': found_data[1]['acceleration_y'],
                'z': found_data[1]['acceleration_z'],
            },
        'temperature': found_data[1]['temperature'],
        'humidity': found_data[1]['humidity'],
        'pressure': found_data[1]['pressure'],
        'battery': found_data[1]['battery']
    }
    client.publish(sensor_topic+found_data[0], payload=json.dumps(msg).encode('UTF-8'))
    print("Tag: {}: {}".format(found_data[0], msg))


if __name__ == "__main__":
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)
    client.loop_start()
    RuuviTagSensor.get_datas(handle_data)
    client.loop_stop()
