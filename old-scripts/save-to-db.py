#!/usr/bin/python3

import os
import numpy as np
import psycopg2
import socket
import struct
import time
import configparser


config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

HOST = config.get('SENSORSERVER',    'HOST', fallback='127.0.0.1')
PORT = config.getint('SENSORSERVER', 'PORT', fallback=10000)

DATABASE = config.get('POSTGRES', 'DATABASE', fallback='sensordata')
DBHOST   = config.get('POSTGRES', 'HOST',     fallback='localhost')
DBUSER   = config.get('POSTGRES', 'USER',     fallback='sensorhub')
DBPASSWD = config.get('POSTGRES', 'PASSWORD', fallback='RaspberrySensors')


SQL = "INSERT INTO sensordata VALUES (%s, TO_TIMESTAMP(%s::double precision), %s, %s)"

print("Connecting to database " + DBUSER + "@" + DBHOST)
conn = psycopg2.connect(host=DBHOST, database=DATABASE, user=DBUSER, password=DBPASSWD)

c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS sensordata
             (mac varchar(17), ts timestamp, type int, value int)''')


#t = ('RHAT',)
#c.execute('SELECT * FROM data WHERE symbol=?', t)

stop = False

HEADER_LENGTH = 6+17+8+1+1

while True:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print("Connecting to %s:%s ".format(HOST, PORT), end='')

        stop = False
        while not stop:
            try:
                s.connect((HOST, PORT))
                print(" connected.")
                stop = True
            except Exception as e:
                print(".", end='', flush=True)
                time.sleep(1)

        stop = False
        data = b''
        while not stop:
            d = s.recv(4096)
            if len(d) > 3000:
                print("Congestion warning while saving data!")
#            print("Got %d bytes" % (len(d)), end='')
            if len(d) < 1:
                print("Connection closed.")
                stop = True
                continue
            data = data + d
#            print(b'read: ' + data)
            while len(data) >= (HEADER_LENGTH):
#                print(data[:HEADER_LENGTH])
                header, mac, timestamp, msg, length = struct.unpack("<6s17sQBB", data[:HEADER_LENGTH])
                mac = mac.decode('ascii')
                print(header, mac, timestamp, msg, length)
                timestamp = timestamp / 1000
                data = data[HEADER_LENGTH:]
#                print(b'data: ' + data)

                if len(data) >= length:
                    if msg == 0x11:
                        coords = struct.unpack("<{:d}H".format(int(length/2)), data[:length])
                        coo = [coords[i:i+2] for i in range(0, len(coords), 2)]
                        for coord in coo:
                            pos = coord[0]<<16
                            pos += coord[1]
                            print(str(coord[0]) + ":" + str(coord[1]) + " :: " + str(pos))
                            c.execute(SQL, (mac, timestamp, msg, pos))

                    elif msg == 0x12:
                        print("Ignoring thermal image")
                    elif msg == 0x21:
                        event = struct.unpack("<B", data[:length])[0]
                        c.execute(SQL, (mac, timestamp, msg, event))
                    elif msg == 0x31:
                        n = struct.unpack("<H", data[:length])[0]
                        c.execute(SQL, (mac, timestamp, msg, n))
                    elif msg == 0x41:
                        co2, status, resistance, voc = struct.unpack("<HBIH", data[:length])
                        c.execute(SQL, (mac, timestamp, msg, voc))
                    elif msg == 0x51:
                        clear, red, green, blue = struct.unpack("<HHHH", data[:length])
                        c.execute(SQL, (mac, timestamp, msg, clear))
                        c.execute(SQL, (mac, timestamp, msg+1, red))
                        c.execute(SQL, (mac, timestamp, msg+2, green))
                        c.execute(SQL, (mac, timestamp, msg+3, blue))
                    elif msg == 0x61:
                        iaq, temperature, humidity, pressure = struct.unpack("<ffff", data[:length])
                        c.execute(SQL, (mac, timestamp, msg, int(iaq*1000)))
                        c.execute(SQL, (mac, timestamp, msg+1, int(temperature*1000)))
                        c.execute(SQL, (mac, timestamp, msg+2, int(humidity*1000)))
                        c.execute(SQL, (mac, timestamp, msg+3, int(pressure*1000)))

                    data = data[length:]
#                    print(b'data2: ' + 	data)
            try:
                conn.commit()
            except Exception as e:
                print(e)
#            print(".")
    time.sleep(1)
    try:
        conn.commit()
    except Exception as e:
        print(e)

print("Quit.")
