#!/usr/local/bin/python3

import os
import json
import numpy as np
from threading import Timer
import serial
import struct
import time
import paho.mqtt.client as mqtt
import configparser
import subprocess
import curses



config = configparser.ConfigParser()
path = os.path.dirname(os.path.realpath(__file__))
config.read(path + "/config.cfg")

MQTTHOST = config.get('MQTT',    'HOST',     fallback='m23.cloudmqtt.com') # The remote host
MQTTPORT = config.getint('MQTT', 'PORT',     fallback=16856)               # The same port as used by the server
USERNAME = config.get('MQTT',    'USERNAME', fallback='testuser')
PASSWORD = config.get('MQTT',    'PASSWORD', fallback='test')

CMDPORT = config.get('TIRADAR',  'CMDPORT',  fallback='COM3')
DATAPORT = config.get('TIRADAR', 'DATAPORT',  fallback='COM4')
CMDPORT_BAUDRATE = config.get('TIRADAR',  'CMDPORT_BAUDRATE',  fallback=115200)
DATAPORT_BAUDRATE = config.get('TIRADAR', 'DATAPORT_BAUDRATE',  fallback=921600)
CHIRP_CONFIG = config.get('TIRADAR', 'CHRIP_CONFIG', fallback='tiradar-config.cfg')

print("Command port; {}@{}".format(CMDPORT, CMDPORT_BAUDRATE))
print("Data port: {}@{}".format(DATAPORT, DATAPORT_BAUDRATE))

normalize = 7
synchronized = False

global persons
last_timestamp = 0
sensor_topic = "sensor/peoplecounter_absolute/tiradar"


client = mqtt.Client()
localclient = mqtt.Client()

quit = False

def timeout(win, line):
    global persons
    global last_timestamp
    if last_timestamp+5 < time.time():
        persons = {}
    msg = {"timestamp": int(time.time()), "count": len(persons), "coords": persons}
    client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
    win.addstr(str(msg) + "\n")
    win.refresh()
    line = line + 1
#    print(msg)
    if not quit:
        t = Timer(3, timeout, [win, line])
        t.start()

def on_mqtt_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    pass

def on_mqtt_message(client, userdata, msg):
    pass



def serialSend(port,  data,  encoding='ascii',  debug=False):
    data = data + '\n'
    data = data.encode(encoding)
    if debug:
        print("Send: {}".format(data))
    port.write(data)
    echo = port.readline()
    done = port.readline()
    prompt = port.read(11)
    if b'Done' not in done:
        rest = port.readline()
        print("ERROR: {}; {}; {}; {}".format(echo,  done,  prompt,  rest))
    else:
        pass
#        print("Result: {}; {}; {}".format(echo,  done,  prompt))





def main(stdscr):
    global persons
    global last_timestamp
    persons = {}
    client.on_connect = on_mqtt_connect
    client.on_message = on_mqtt_message

    client.username_pw_set(USERNAME, PASSWORD)
    client.connect_async(MQTTHOST, MQTTPORT)
    client.loop_start()

    curses.curs_set(0)
    debugwin = curses.newwin(13, 200, 0, 0)
    errwin = curses.newwin(5, 200, 13, 0)
    errwin.scrollok(True)
    errwin.idlok(True)
    coordwin = curses.newwin(30, 200, 18, 0)
    coordwin.scrollok(True)
    coordwin.idlok(True)

    line = 0
    t = Timer(3, timeout, [coordwin, line])

    try:
        subprocess.run('/home/pi/software/myxds110reset/myxds110reset')
        time.sleep(5)
        with serial.Serial(CMDPORT, CMDPORT_BAUDRATE,  timeout=1, rtscts=False, dsrdtr=False) as commandPort:
            errwin.addstr("Stop sensor\n")
            serialSend(commandPort,  'sensorStop')

            errwin.addstr("Config/Start!\n")
            with open(CHIRP_CONFIG) as c:
                for line in c:
                    if not line.startswith('#'):
                        serialSend(commandPort, line.rstrip())

            t.start()
            SYNC = b'\x02\x01\x04\x03\x06\x05\x08\x07'

            synchronized = False
            stop = False
            with serial.Serial(DATAPORT, DATAPORT_BAUDRATE, timeout=10, rtscts=False, dsrdtr=False) as dataPort:
                while not stop:
                    if not synchronized:
                        s = b''
                        index = 0
                        s = dataPort.read(1)
                        if len(s) == 0:
                            continue
                        while s[index] == SYNC[index] and index < len(SYNC)-1:
                            s = s + dataPort.read(1)
#                            print(s)
                            index = index + 1
                        if s == SYNC:
                            synchronized = True

                    else:
                        header = s + dataPort.read(52-len(s))
                        s = b''
                        if len(header) == 52:
                            arr = np.array(struct.unpack('<26H',  header))
                            crc = np.sum(arr)
                            crc = np.sum(struct.unpack('<2H', crc))
                            if crc == 0xffff:
                                try:
                                    sync,  version,  platform,  timestamp,  packetLength,  \
                                    framenumber,  subframeNumber, chirpMargin,  frameMargin,  \
                                    uartSentTime,  trackProcessTime,  numTLVs,  checksum = \
                                        struct.unpack('<QIIIIIIIIIIHH',  header)
                                    debugwin.erase()
                                    debugwin.addstr(0, 0, "Sync: {}".format(hex(sync)))
                                    debugwin.addstr(1, 0, "Version: {}".format(hex(version)))
                                    debugwin.addstr(2, 0, "Platform: {}".format(hex(platform)))
                                    debugwin.addstr(3, 0, "Timestamp: {}".format(timestamp))
                                    debugwin.addstr(4, 0, "Packet length: {}".format(packetLength))
                                    debugwin.addstr(5, 0, "Frame: {}".format(framenumber))
                                    debugwin.addstr(6, 0, "Subframe: {}".format(subframeNumber))
                                    debugwin.addstr(7, 0, "Chirp margin: {}".format(chirpMargin))
                                    debugwin.addstr(8, 0, "Frame margin: {}".format(frameMargin))
                                    debugwin.addstr(9, 0, "UART sent time: {}".format(uartSentTime))
                                    debugwin.addstr(10, 0, "Track processing time: {}".format(trackProcessTime))
                                    debugwin.addstr(11, 0, "Num TLVs: {}".format(numTLVs))
                                    debugwin.addstr(12, 0, "Checksum: {}".format(hex(checksum)))
                                    debugwin.refresh()
                                    packetLength = packetLength - 52
                                    if (packetLength > 0):
                                        data = dataPort.read(packetLength)
        #                                print("len: {} vs {}".format(packetLength,  len(data)))
                                        offset = 0
                                        for tlv in range(numTLVs):
                                            tlv_type, tlv_length = struct.unpack('<II',  data[offset:offset+8])
        #                                    print("TLV: {} {} {}".format(tlv_type,  tlv_length,  offset))
                                            if tlv_type == 0x06:        # Point cloud
                                                pass
                                            if tlv_type == 0x07:        # Target object list
        #                                        print(len(data[offset+8:offset+tlv_length]))
                                                persons = {
                                                    x[0]: {
                                                        'x': ((x[1] +2) / 8),
                                                        'y': ((x[2]   ) / 8)
                                                    } for x in struct.iter_unpack('<Iffffff9ff',  data[offset+8:offset+tlv_length])}
                                                msg = {"timestamp": int(time.time()), "count": len(persons), "coords": persons}
                                                last_timestamp = msg['timestamp']
                                                coordwin.addstr(str(msg)+"\n")
#                                                coordwin.addstr("{}: {}, {}".format(x[0], x[1], x[2]))
                                                coordwin.refresh()
                                                client.publish(sensor_topic, payload=json.dumps(msg).encode('UTF-8'))
                                            if tlv_type == 0x08:        # Target index
                                                pass
                                            offset = offset+tlv_length
                                except Exception as e:
                                    synchronized = False
                                    errwin.addstr("Error decoding frame: {}\n".format(e))
                                    errwin.refresh()
                            else:
                                synchronized = False
                                errwin.addstr("CRC Error: {}\n".format(crc))
                                if header:
                                    errwin.addstr(str(header)+"\n")
                                errwin.refresh()
                        else:
                            errwin.addstr("Header length mismatch: {} v.s. 52 bytes\n".format(len(header)))
                            errwin.refresh()
                            synchronized = False
    except Exception as e:
        errwin.addstr("Error: {}\n".format(e))
        errwin.refresh()
        client.loop_stop()
        quit = True
        t.cancel()


    client.loop_stop()



if __name__ == "__main__":
    curses.wrapper(main)
