from datetime import datetime, timedelta
import json
import logging
import numpy as np
import os
import signal
import socket
import subprocess
import sys
import time
from threading import Timer
import uuid
from mqtt_client import MQTTClient
from omxplayer.player import OMXPlayer

DALI_PRO = ('192.168.11.244', 3416)


class Mediaplayer(MQTTClient):

    _datasources_topic = 'geofence/#'

    def __init__(self, name, configfile=None):
        MQTTClient.__init__(self, name, configfile)
        self.load_config(name)
        self._next_start_at_earliest = datetime.now()
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._playing = False

    def load_config(self, name):
        changed = False
        if self.config.has_section(name):
            self._uuid = self.config.get(self.name,          'UUID').value
            self._script = self.config.get(self.name,        'SCRIPT').value
            self._trigger_topic = self.config.get(self.name, 'trigger_topic').value
            self._file = self.config.get(self.name,          'file').value
            self._player = OMXPlayer(self._file, args=['--no-osd', '--no-keys', '-b', '--video_fifo', '10'])
            self._playing = True
        else:
            print('ERROR: device configuration not found!')
            self.config.add_section(name)
            self.config[name].add_before.space()
            self.config.set(name, 'SCRIPT', os.path.basename(sys.argv[0]))
            self.config.set(name, 'UUID', uuid.uuid4())
            self.config.set(name, 'trigger_topic', '<topic>')
            self.config.set(name, 'file', '<file/name>')
            self.save_config()
            print('Default device configuration written to config file. Please change!')
            raise 'Please change device configuration NOW!'

    def start(self):
        self.mqtt_start()
#        self.mqtt_client.message_callback_add(self._datasources_topic + '/' + self._trigger_topic, self.trigger_received)
        self.mqtt_client.message_callback_add('geofence/7a0c18cd-3f8b-434a-a049-626db4f37523', self.trigger_received)
#        self.mqtt_client.subscribe(self._datasources_topic)
        self.check_timeout()
        while True:
            time.sleep(1)
            self.mqtt_client.subscribe(self._datasources_topic)

    def stop(self):
        print("Stopping..")
        self.mqtt_stop()

    def check_timeout(self):
        self._check_timer = Timer(1, self.check_timeout)
        self._check_timer.start()
        playing = None
        try:
            playing = self._player.playback_status()
        except Exception as e:
            pass
        if not playing and self._playing:
            self._playing = False
            self.udp_socket.sendto('Demo Event 1 DALIPro.i00'.encode('ascii'), DALI_PRO)
            msg = {
                'dim': 0,
                'src': self._uuid,
            }
            self.mqtt_client.publish('light', payload=json.dumps(msg).encode('ascii'))
            print("STOP!!")
#        subprocess.call("sudo /usr/bin/tvservice -o", shell=True)

    def trigger_received(self, client, userdata, msg):
        print("\n\n\nGot a message: " + msg.topic)
        message = json.loads(msg.payload.decode('UTF-8'))
        if self._next_start_at_earliest < datetime.now() and message.get('fence') == 'fence_projector2':
            self._check_timer.cancel()
            self.udp_socket.sendto('Demo Event 1 DALIPro.i01'.encode('ascii'), DALI_PRO)
            # Dimming: "Demo Event <hex-value> DALIPro.i02.1"
            msg = {
                'dim': 50,
                'src': self._uuid,
            }
            self.mqtt_client.publish('light', payload=json.dumps(msg).encode('ascii'))
            print("play!")
#            subprocess.call("sudo /usr/bin/tvservice -p", shell=True)
            self._player.load(self._file)
            self._playing = True
            runtime = self._player.duration()
            self._next_start_at_earliest = datetime.now() + timedelta(seconds=runtime) + timedelta(seconds=2)
            self.check_timeout()
            print(runtime)
            print(self._next_start_at_earliest)


if __name__ == '__main__':
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    mediaplayer = Mediaplayer(sys.argv[1])

#    def signal_handler(signal_frame, nope):
#        def run(*args):
#            print('Stopping Geofence')
#            geofence.stop()
#        Thread(target=run).start()

    signal.signal(signal.SIGINT, quit)

    mediaplayer.start()
