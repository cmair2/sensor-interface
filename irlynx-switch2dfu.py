#!/usr/local/bin/python3

import os
import serial
import configparser

config = configparser.ConfigParser()

SERIALPORT = config.get('IRLYNX', 'SERIALPORT', fallback='/dev/ttyUSB0')
BAUDRATE = config.get('IRLYNX',   'BAUDRATE', fallback=19200)

if __name__ == "__main__":
    success = False
    try:
        print('Connecting to {}...'.format(SERIALPORT))
        with serial.Serial(SERIALPORT, BAUDRATE) as ser:
            ser.write(b'\x13\x00')
            ser.flush()
            answer = ser.read(1)
            print(answer)
            while not (answer == b'\x13'):
                answer = ser.read(1)
                print(answer)
            answer = ser.read(3)
            if answer == b'\x02\x4F\x4B':
                print("Success. Sensor is in DFU mode now!")
                success = True
            else:
                print("Switch to DFU mode failed! Answer was: {}".format(answer))
    except Exception as e:
        print(e)
    if success:
        cmd = 'stm32flash -w {} {} -R -b 115200 -e 10'.format(firmware, SERIALPORT)
        print('Starting flash process: {}'.format(cmd))
        print('Press CTRL-C to interrupt within the next 5 seconds.')
        sleep(6)
        subprocess.run(cmd, shell=True)
