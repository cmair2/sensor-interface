#!/usr/bin/env python3

import json
import uuid
import paho.mqtt.client as mqtt
import time
import sys
import os
import numpy as np
from mqtt_client import MQTTClient
from configupdater import ConfigUpdater


class GenericSensor(MQTTClient):

    _mqtt_topic_prefix = 'sensor'

    raw_image_rescale = False
    raw_image_invert = False
    processing_delay = 0
    sensor_type = 'unknown'


    def __init__(self, name, configfile=None):
        MQTTClient.__init__(self, name, configfile)
        self._load_device_config(name)

    def _load_device_config(self, name):
        if self.config.has_section(name):
            self.vendor = self.config.get(self.name,     'VENDOR').value
            self.model = self.config.get(self.name,      'MODEL').value
            self.technology = self.config.get(self.name, 'TECHNOLOGY').value
            self._uuid = self.config.get(self.name,      'UUID').value
            self._script = self.config.get(self.name,    'SCRIPT').value
        else:
            print('ERROR: device configuration not found!')
            self.config.add_section(name)
            self.config[name].add_before.space()
            self.config.set(name, 'VENDOR', 'Unknown (Osram)')
            self.config.set(name, 'MODEL', 'Unknown (Device name)')
            self.config.set(name, 'TECHNOLOGY', 'Unknown (10GHz UWB Radar)')
            self.config.set(name, 'SCRIPT', os.path.basename(sys.argv[0]))
            self.config.set(name, 'UUID', uuid.uuid4())
            self.save_config()
            print('Default device configuration written to config file. Please change!')
            raise 'Please change device configuration NOW!'

        self._mqtt_topic = self._mqtt_topic_prefix + '/' + self.sensor_type + '/' + self._uuid

    def publish(self, payload):
        if 'rawData' in payload:
            raw = payload['rawData']
            if self.raw_image_rescale:
                raw = np.array(raw)
                ptp = np.ptp(raw)
                if ptp > 0:
                   raw = (raw - np.min(raw)) / ptp
            if self.raw_image_invert:
                raw = 1 - np.array(raw)
            payload['rawData']= raw.tolist()
        msg = {
            "vendor": self.vendor,
            "model": self.model,
            "technology": self.technology,
            "timestamp": time.time(),
            "processingDelay": self.processing_delay,
            **payload
        }
        self.mqtt_client.publish(self._mqtt_topic, payload=json.dumps(msg).encode('ascii'))
