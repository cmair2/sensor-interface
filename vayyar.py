import json
import numpy as np
import signal
from collections import OrderedDict
from struct import unpack_from
import sys
import websocket
from threading import Thread, Timer
from generic_sensor import GenericSensor


class VayyarPeoplecounter(GenericSensor):

    raw_image_invert = True
    raw_image_rescale = True
    sensor_type = 'peoplecounter_absolute'

    _DEFAULT_SENSOR_PARAMETERS = {
        'Cfg.MonitoredRoomDims': [-2, 2, -2, 2, 0.7, 1.8],
        'Cfg.Common.sensorOrientation.mountPlane': 'xy',
        'Cfg.Common.sensorOrientation.transVec': [0.0, 0.0, 2.7],
        'Cfg.imgProcessing.substractionMode': 6.0,
        'Cfg.TargetProperties.MaxPersonsInArena': 10.0,
        'Cfg.TargetProperties.StandingMaxHeight': 2.0,
        'Cfg.TargetProperties.StandingMinHeight': 1.55,
        'Cfg.TargetProperties.SittingMinHeight': 0.8,
        'Cfg.TargetProperties.LyingMinHeight': 0.2,
        'Cfg.TargetProperties.PersonRadius': 0.5,
        'MPR.save_dir': '',
        'MPR.read_from_file': 0.0,
        'MPR.save_to_file': 0.0,
        'MPR.save_image_to_file': 0.0,
        'Cfg.OutputData.save_to_file': 0.0,
        'Cfg.ExternalGUI.FilterImage.TH': 0.0,
        'Cfg.ExternalGUI.FilterImage.numOfSd': 3.0,
        'Cfg.PeopleCounter.inCarIsLocked': False,
    }


    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self.load_config()
        self.persons = None
        self.dimensions = []
        self._timeoutcnt = 0
        self._stop_poll_timer = False
        self.poll_timer = Timer(1, self.poll_timer_timeout)


    def load_config(self):
        self.host = self.load_config_value(self.name, 'host', default='10.42.0.68')
        self.port = self.load_config_value(self.name, 'port', default='1234')

    def on_open(self, ws):
        print('Connected to sensor.')
        self.mqtt_start()

        # Send a message to (re-)initialize listeners to a known state
        self.publish({
            "count": 0,
            "coords": {}
        })

        if self.config.has_option(self.name, 'CONFIG'):
            sensor_config = json.loads(self.config.get(self.name, 'CONFIG').value)
            print(sensor_config)
        else:
            print('ERROR loading sensor configuration. Using defaults.')
            sensor_config = self._DEFAULT_SENSOR_PARAMETERS
            config_string = json.dumps(sensor_config, sort_keys=True, indent=4)
            # Add two spaces in front of closing '}'so that the ini-file parser
            # correctly recognizes the end of the multi-line json string
            config_string = config_string[:-1] + '  ' + config_string[-1:]
            self.config.set(self.name, 'CONFIG', config_string)
            self.save_config()

        print('Sending configuration...')
        print(json.dumps(sensor_config, sort_keys=True, indent=4))
        self.ws.send(json.dumps(
                {"Type": "COMMAND",
                 "ID": "SET_PARAMS",
                 "Payload": sensor_config,
                }).encode("ascii"))
        self.ws.send(json.dumps({
                'Type': 'COMMAND',
                'ID': 'SET_OUTPUTS',
                'Payload': {
                     # possible outputs:
                     # 'I', 'Q', 'pairs', 'freqs',
                     # 'rawImage_XYZ', 'rawImage_XY', 'rawImage_XZ', 'rawImage_YZ'
                     # 'LocationMatrix'
                     #'binary_outputs': ['I', 'Q', 'pairs', 'freqs', 'LocationMatrix', 'NumOfPeople'],
                    'binary_outputs': ['LocationMatrix', 'NumOfPeople', 'rawImage_XY', 'BreathingMatrix'],
                    #'binary_outputs': ['rawImage_XYZ']
                    # json outputs must not be conigured from API when GUI is running (GUI uses json outputs)
                    'json_outputs': ['PostureVector', 'NumOfPeople']
                }
            }).encode("ascii"))
        self.start_sensor()
        print('Requesting new data')
        self.request_data()


    def on_close(self, ws):
        print('Websocket closed. Stopping MQTT.')
        self.mqtt_stop()


    def on_error(self, ws, error):
        print('Websocket Error: ' + str(error))
        try:
            self.ws.close()
        except Exception as e:
            print(e)
        self.connect_to_sensor()

    def start_sensor(self):
        print('Starting sensor...')
        self.ws.send(json.dumps({
                        "Type": "COMMAND",
                        "ID": "START",
                        "Payload": {}
                     }).encode("ascii"))
        self.poll_timer.cancel()
        self.poll_timer = Timer(1, self.poll_timer_timeout)
        self.poll_timer.start()

    def stop_sensor(self):
        print('Stopping sensor...')
        self.ws.send(json.dumps({
                        "Type": "COMMAND",
                        "ID": "STOP",
                     }).encode("ascii"))

    def request_data(self):
        self.ws.send(json.dumps({
                        "Type": "QUERY",
                        "ID": "JSON_DATA"
                     }).encode("ascii"))
        self.ws.send(json.dumps({
                        "Type": "QUERY",
                        "ID": "BINARY_DATA"
                     }).encode("ascii"))

    def poll_timer_timeout(self):
        print('Persons: {}'.format(self.persons))
        self._timeoutcnt
        if (self.persons is not None) or (self._timeoutcnt > 5):
            self.persons = None
            self._timeoutcnt = 0
            self.request_data()
        self._timeoutcnt = self._timeoutcnt + 1

#        msg = {"timestamp": time.time(), "count": len(persons), "coords": persons}
#        client.publish(mqtt_topic, payload=json.dumps(msg).encode('UTF-8'))
#        print(msg)
        if not self._stop_poll_timer:
            self.poll_timer = Timer(0.25, self.poll_timer_timeout)
            self.poll_timer.start()


    def parse_message(self, buffer):
        """ This section parse Vayyar messages from JSON / Vayyar internal binary format between sensor and client
        """
        DTYPES = {
            0: np.int8,
            1: np.uint8,
            2: np.int16,
            3: np.uint16,
            4: np.int32,
            5: np.uint32,
            6: np.float32,
            7: np.float64,
        }

        ASCII_RS = '\u001e'
        ASCII_US = '\u001f'

        if isinstance(buffer, str):
            return json.loads(buffer)
        seek = 0
        # bufferSize = unpack_from('i', buffer, seek)[0]
        fields_len = unpack_from('i', buffer, seek + 4)[0]
        fields_split = unpack_from(str(fields_len) + 's', buffer, seek + 8)[0].decode('utf8').split(ASCII_RS)
        msg = {'ID': fields_split[0], 'Payload': OrderedDict.fromkeys(fields_split[1].split(ASCII_US))}
        seek += 8 + fields_len
        for key in msg['Payload']:
            # fieldSize = np.asscalar(np.frombuffer(buffer, np.int32, 1, seek))
            seek += np.int32().nbytes
            dtype = DTYPES[np.asscalar(np.frombuffer(buffer, np.int32, 1, seek))]
            seek += np.int32().nbytes
            ndims = np.asscalar(np.frombuffer(buffer, np.int32, 1, seek))
            seek += np.int32().nbytes
            dims = np.frombuffer(buffer, np.int32, ndims, seek)
            seek += ndims * np.int32().nbytes
            data = np.frombuffer(buffer, dtype, np.prod(dims), seek)
            seek += np.prod(dims) * dtype().nbytes
            msg['Payload'][key] = data.reshape(dims) if ndims else np.asscalar(data)
        return msg


    def on_message(self, ws, message):
        m = self.parse_message(message)
#        print(m)
        if m['ID'] == "SET_PARAMS":
            self.dimensions = dict(zip(["xmin", "xmax", "ymin", "ymax", "zmin", "zmax"], m['Payload']['Cfg.MonitoredRoomDims']))
            print('Arena dimensions: {}'.format(self.dimensions))
            self.start_sensor()

        if m['ID'] == "GET_STATUS":
            print('on_message: {}'.format(m))
            if m['Payload']['status'] == "WAITING_FOR_START":
                self.start_sensor()
            if m['Payload']['status'] == "IMAGING":
#                self.poll_timer = Timer(1, self.poll_timer_timeout)
#                self.poll_timer.start()
                pass
            if m['Payload']['status'] == "ERROR":
                print("ERROR: {}".format(m['Payload']['errorMessage']))
        if m["ID"] == "JSON_DATA":
            msg = {
               "postures": dict((c, x.lower()) for c, x in enumerate(m['Payload']['PostureVector']) if c < int(m['Payload']['NumOfPeople']))
            }
            self.publish(msg)

#            print(m['Payload']['NumOfPeople'])
#            print('-'*10)
#            print(m)
#            print(m['Payload']['LocationMatrix'])
#            for c, a in enumerate(m['Payload']['LocationMatrix']):
#                print("{}: {}".format(c, a))
#            print(m['Payload']['rawImage_XY'])
#             pass

        if m["ID"] == "BINARY_DATA":
            self._timeoutcnt = 0
            if len(self.dimensions) <= 0:
                print('Got data with unknown dimensions. Ignoring.')
#                with open("vayyar-log.log", "a") as f:
#                    f.write("dimensions is not defined. quitting: {}: {}\n".format(datetime.datetime.now(), m["ID"]))
#                signal_handler(None, None) # Call signal handler. It will stop the sensor and quit.
                return
            msg = {
               "count": int(m['Payload']['NumOfPeople']),
               "coords": {c: {"x": (x-self.dimensions['xmin'])/(self.dimensions['xmax']-self.dimensions['xmin']),
                              "y": (y-self.dimensions['ymin'])/(self.dimensions['ymax']-self.dimensions['ymin'])
                             } for c, (x, y, z) in enumerate(m['Payload']['LocationMatrix']) if (x != "NaN" and y != "NaN") and not np.isnan(x) and not np.isnan(y)
                         },
#               "postures": dict((c, x.lower()) for c, x in enumerate(m['Payload']['PostureVector']) if c < int(m['Payload']['NumOfPeople'])),
               "dimensions": self.dimensions,
               "rawData": m['Payload']['rawImage_XY'],
               "breathingData": np.array(m['Payload']['BreathingMatrix']).tolist(),
            }
#            print(msg)
            self.persons = msg['count']
#            self.persons = len(msg['coords'])
            self.publish(msg)

    def connect_to_sensor(self):
        url = "ws://{}:{}".format(self.host, self.port)
        print('Connecting to {}'.format(url))
        self.ws = websocket.WebSocketApp(url,
                                        on_open = self.on_open,
                                        on_message = self.on_message,
                                        on_error = self.on_error,
                                        on_close = self.on_close
                            )

    def start(self):
        self.connect_to_sensor()
        print('run forever')
        self.ws.run_forever()
        print('Websocket connection dropped. Stopping.')
        self._stop_poll_timer = True
        self.poll_timer.cancel()

    def stop(self):
        self.stop_sensor()
        self._stop_poll_timer = True
        self.poll_timer.cancel()
        self.ws.close()



if __name__ == "__main__":
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    vayyar = VayyarPeoplecounter(sys.argv[1])

    def signal_handler(signal, frame):
        def run(*args):
            print('Stopping sensor...')
            vayyar.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    Thread(target=vayyar.start).start()
