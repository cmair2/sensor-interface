import curses
import json
import signal
import serial
import struct
import subprocess
import sys
import time
import numpy as np
from threading import Thread, Timer
from generic_sensor import GenericSensor


class TiRadarPeoplecounter(GenericSensor):

    raw_image_invert = False
    raw_image_rescale = True
    coordinates_normalize = 7
    sensor_type = 'peoplecounter_absolute'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self.load_config()
        self.persons = {}
        self.dimensions = []
        self.raw_image = b''
        self._stop_reading_sensor = False
        self._stop_report_timer = False
        self._stop_raw_timer = False
        self.raw_timer = None
        self.report_timer = None
        self.last_timestamp = time.time()

    def load_config(self):
        self.cmdport = self.load_config_value(self.name, 'cmdport', default='/dev/ttyACM1')
        self.dataport = self.load_config_value(self.name, 'dataport', default='/dev/ttyACM2')
        self.cmdport_baudrate = self.load_config_value(self.name, 'cmdport_baudrate', default='115200')
        self.dataport_baudrate = self.load_config_value(self.name, 'dataport_baudrate', default='921600')
        self.chirp_config_file = self.load_config_value(self.name, 'chrip_config_file', default='tiradar.cfg')

    def publish_sensor_data(self):
        self.report_timer_restart(5, self.publish_sensor_data)
        if self.last_timestamp+5 < time.time():
            self.last_timestamp = time.time()
            self.persons = {}

        msg = {
            "count": len(self.persons),
            "coords": self.persons
        }
        #print(msg)
        self.publish(msg)

        self.coordwin.addstr(str(msg) + "\n")
        self.coordwin.refresh()
        self.line = self.line + 1

    def report_timer_restart(self, timeout, method):
        if self.report_timer:
            self.report_timer.cancel()
        if not self._stop_report_timer:
            self.report_timer = Timer(timeout, method)
            self.report_timer.start()

    def start_timers(self, ser):
        self.report_timer = Timer(5, self.publish_sensor_data)
        self.report_timer.start()

    def start(self):
        self.mqtt_start()
        curses.wrapper(self.read_sensor)

    def stop(self):
        self._stop_reading_sensor = True
        self._stop_report_timer = True
        if self.report_timer:
            self.report_timer.cancel()
        self.mqtt_stop()

    def serialSend(self, port, data, encoding='ascii', debug=False):
        data = data + '\n'
        data = data.encode(encoding)
        if debug:
            print("Send: {}".format(data))
        port.write(data)
        echo = port.readline()
        done = port.readline()
        prompt = port.read(11)
        if b'Done' not in done:
            rest = port.readline()
            print("ERROR: {}; {}; {}; {}".format(echo,  done,  prompt,  rest))
        else:
            pass
#            print("Result: {}; {}; {}".format(echo,  done,  prompt))

    def read_sensor(self, stdscr):
        curses.curs_set(0)
        self.debugwin = curses.newwin(13, 200, 0, 0)
        self.errwin = curses.newwin(5, 200, 13, 0)
        self.errwin.scrollok(True)
        self.errwin.idlok(True)
        self.coordwin = curses.newwin(30, 200, 18, 0)
        self.coordwin.scrollok(True)
        self.coordwin.idlok(True)

        self.line = 0

        try:
            subprocess.run('/home/pi/software/myxds110reset/myxds110reset')
            time.sleep(5)
            with serial.Serial(self.cmdport, self.cmdport_baudrate, timeout=1, rtscts=False, dsrdtr=False) as commandPort:
                self.errwin.addstr("Stop sensor\n")
                self.serialSend(commandPort, 'sensorStop')

                self.errwin.addstr("Config/Start!\n")
                with open(self.chirp_config_file) as c:
                    for line in c:
                        if not line.startswith('#'):
                            self.serialSend(commandPort, line.rstrip())

                self.publish_sensor_data()
                SYNC = b'\x02\x01\x04\x03\x06\x05\x08\x07'

                synchronized = False
                with serial.Serial(self.dataport, self.dataport_baudrate, timeout=10, rtscts=False, dsrdtr=False) as dataPort:
                    while not self._stop_reading_sensor:
                        if not synchronized:
                            s = b''
                            index = 0
                            s = dataPort.read(1)
                            #print(s)
                            if len(s) == 0:
                                continue
                            while s[index] == SYNC[index] and index < len(SYNC)-1:
                                s = s + dataPort.read(1)
                                #print(s)
                                index = index + 1
                            if s == SYNC:
                                synchronized = True

                        else:
                            header = s + dataPort.read(52-len(s))
                            s = b''
                            if len(header) == 52:
                                arr = np.array(struct.unpack('<26H',  header))
                                crc = np.sum(arr)
                                crc = np.sum(struct.unpack('<2H', crc))
                                if crc == 0xffff:
                                    try:
                                        sync,  version,  platform,  timestamp,  packetLength,  \
                                        framenumber,  subframeNumber, chirpMargin,  frameMargin,  \
                                        uartSentTime,  trackProcessTime,  numTLVs,  checksum = \
                                            struct.unpack('<QIIIIIIIIIIHH',  header)
                                        self.debugwin.erase()
                                        self.debugwin.addstr(0, 0, "Sync: {}".format(hex(sync)))
                                        self.debugwin.addstr(1, 0, "Version: {}".format(hex(version)))
                                        self.debugwin.addstr(2, 0, "Platform: {}".format(hex(platform)))
                                        self.debugwin.addstr(3, 0, "Timestamp: {}".format(timestamp))
                                        self.debugwin.addstr(4, 0, "Packet length: {}".format(packetLength))
                                        self.debugwin.addstr(5, 0, "Frame: {}".format(framenumber))
                                        self.debugwin.addstr(6, 0, "Subframe: {}".format(subframeNumber))
                                        self.debugwin.addstr(7, 0, "Chirp margin: {}".format(chirpMargin))
                                        self.debugwin.addstr(8, 0, "Frame margin: {}".format(frameMargin))
                                        self.debugwin.addstr(9, 0, "UART sent time: {}".format(uartSentTime))
                                        self.debugwin.addstr(10, 0, "Track processing time: {}".format(trackProcessTime))
                                        self.debugwin.addstr(11, 0, "Num TLVs: {}".format(numTLVs))
                                        self.debugwin.addstr(12, 0, "Checksum: {}".format(hex(checksum)))
                                        self.debugwin.refresh()
                                        packetLength = packetLength - 52
                                        if (packetLength > 0):
                                            data = dataPort.read(packetLength)
                                            #self.coordwin.addstr("len: {} vs {}\n".format(packetLength,  len(data)))
                                            #self.coordwin.refresh()
                                            offset = 0
                                            for tlv in range(numTLVs):
                                                #self.coordwin.addstr("{}, {}\n".format(len(data), offset))
                                                tlv_type, tlv_length = struct.unpack('<II',  data[offset:offset+8])
                                                #self.coordwin.addstr("TLV: {} {} {}\n".format(tlv_type,  tlv_length,  offset))
                                                #self.coordwin.refresh()
                                                if tlv_type == 0x06:        # Point cloud
                                                    pass
                                                if tlv_type == 0x07:        # Target object list
                                                    #print(len(data[offset+8:offset+tlv_length]))
                                                    self.persons = {
                                                        x[0]: {
                                                            'x': ((x[1] +2) / 8),
                                                            'y': ((x[2]   ) / 8)
                                                        } for x in struct.iter_unpack('<Iffffff9ff',  data[offset+8:offset+tlv_length])}
                                                    self.publish_sensor_data()
                                                if tlv_type == 0x08:        # Target index
                                                    pass
                                                offset = offset+tlv_length
                                    except Exception as e:
                                        synchronized = False
                                        self.errwin.addstr("Error decoding frame: {}\n".format(e))
                                        self.errwin.refresh()
                                else:
                                    synchronized = False
                                    self.errwin.addstr("CRC Error: {}\n".format(crc))
                                    if header:
                                        self.errwin.addstr(str(header)+"\n")
                                    self.errwin.refresh()
                            else:
                                self.errwin.addstr("Header length mismatch: {} v.s. 52 bytes\n".format(len(header)))
                                self.errwin.refresh()
                                synchronized = False
        except Exception as e:
            self.errwin.addstr("Error: {}\n".format(e))
            self.errwin.refresh()
            self.stop()
            time.sleep(5)


if __name__ == "__main__":
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    tiradar = TiRadarPeoplecounter(sys.argv[1])

    def signal_handler(signal, frame):
        def run(*args):
            print('Stopping sensor...')
            tiradar.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    Thread(target=tiradar.start).start()
