import json
import logging
import numpy as np
import os
import signal
import sys
import time
import uuid
from threading import Timer
from mqtt_client import MQTTClient


class Geofence(MQTTClient):

    _datasources_topic = 'sensor/peoplecounter_absolute/#'
    _datasink_topic = 'geofence'

    def __init__(self, name, configfile=None):
        MQTTClient.__init__(self, name, configfile)
        self.load_config(name)

    def load_config(self, name):
        changed = False
        if self.config.has_section(name):
            self._uuid = self.config.get(self.name,      'UUID').value
            self._script = self.config.get(self.name,    'SCRIPT').value
        else:
            print('ERROR: device configuration not found!')
            self.config.add_section(name)
            self.config[name].add_before.space()
            self.config.set(name, 'SCRIPT', os.path.basename(sys.argv[0]))
            self.config.set(name, 'UUID', uuid.uuid4())
            self.save_config()
            print('Default device configuration written to config file. Please change!')
            raise 'Please change device configuration NOW!'

        section = self.config[self.name]
        comments = [str(x) for x in section if str(x).startswith('# fence_<name>')]
        if not comments:
            print('Add comment')
            self.config[name].add_after.comment('fence_<name>: x1, y1, x2, y2')
            changed = True
        if changed:
            self.save_config()

        self.fences = [k for k in section.to_dict() if str(k).startswith('fence_')]
        print(self.fences)

    def start(self):
        self.mqtt_start()
        self.mqtt_client.message_callback_add(self._datasources_topic, self.peoplecounter_absolute_report)
#        self.mqtt_client.subscribe(self._datasources_topic)
        while True:
            time.sleep(1)
            self.mqtt_client.subscribe('#')

    def stop(self):
        print("Stopping..")
        self.mqtt_stop()

    def peoplecounter_absolute_report(self, client, userdata, msg):
        print("\n\n\nGot a message: " + msg.topic)
        message = json.loads(msg.payload.decode('UTF-8'))
        coords = message['coords']
        section = self.config[self.name]
        for fence in self.fences:
            bounds = np.array(section[fence].value.split(','), np.float)
            print(bounds)
            inside = {a: coords[a] for a in coords if coords[a]['x'] > bounds[0] and coords[a]['y'] > bounds[1] and coords[a]['x'] < bounds[2] and coords[a]['y'] < bounds[3]}
            if len(inside) > 0:
                print(inside)
                print(fence)
                payload = {
                    "uuid": self._uuid,
                    "fence": fence,
                    "source": msg.topic,
                    "cause": inside,
                }
                self.mqtt_client.publish(self._datasink_topic + '/' + self._uuid, payload=json.dumps(payload).encode('ascii'))


if __name__ == '__main__':
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    geofence = Geofence(sys.argv[1])

#    def signal_handler(signal_frame, nope):
#        def run(*args):
#            print('Stopping Geofence')
#            geofence.stop()
#        Thread(target=run).start()

    signal.signal(signal.SIGINT, quit)

    geofence.start()
