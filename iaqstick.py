import logging
import os
import signal
import sys
from threading import Timer
import usb.core
import usb.util
from generic_sensor import GenericSensor


logger = logging.getLogger('iAQ_Stick')

class IAQStick(GenericSensor):

    sensor_type = 'ambientair'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self.load_config()
        self._pids = {}
        self._check_timer = Timer(2, self.timer_timeout)


    def load_config(self):
        pass

    def start(self):
        self._dev = usb.core.find(idVendor=0x03eb, idProduct=0x2013)
        if self._dev is None:
            logger.error('iaqstick: iAQ Stick not found')
            return
        self._intf = 0
        self._type1_seq = 0x0001
        self._type2_seq = 0x67

        try:
            if self._dev.is_kernel_driver_active(self._intf):
                self._dev.detach_kernel_driver(self._intf)

            self._dev.set_configuration(0x01)
            usb.util.claim_interface(self._dev, self._intf)
            self._dev.set_interface_altsetting(self._intf, 0x00)

            manufacturer = usb.util.get_string(self._dev, self._dev.iManufacturer)
            product = usb.util.get_string(self._dev, self._dev.iProduct)
            logger.info('iaqstick: Manufacturer: {} - Product: {}'.format(manufacturer, product))
            ret = self.xfer_type1('*IDN?')
            #print(ret)
            self._dev.write(0x02, bytes('@@@@@@@@@@@@@@@@', 'utf-8'), 1000)
            ret = self.xfer_type1('KNOBPRE?')
            #print(ret)
            ret = self.xfer_type1('WFMPRE?')
            #print(ret)
            ret = self.xfer_type1('FLAGS?')
            #print(ret)
        except Exception as e:
            logger.error("iaqstick: init interface failed - {}".format(e))

        self.mqtt_start()
        self._check_timer.start()

    def stop(self):
        self.mqtt_stop()
        self._check_timer.cancel()

        try:
            usb.util.release_interface(self._dev, self._intf)
        except Exception as e:
            logger.error("iaqstick: releasing interface failed - {}".format(e))


    def timer_timeout(self):
        self._update_values()
        self._check_timer = Timer(2, self.timer_timeout)
        self._check_timer.start()


    def xfer_type1(self, msg):
        out_data = bytes('@{:04X}{}\n@@@@@@@@@@'.format(self._type1_seq, msg), 'utf-8')
        self._type1_seq = (self._type1_seq + 1) & 0xFFFF
        ret = self._dev.write(0x02, out_data[:16], 1000)
        in_data = bytes()
        while True:
            ret = bytes(self._dev.read(0x81, 0x10, 1000))
            if len(ret) == 0:
                break
            in_data += ret
        return in_data.decode('iso-8859-1')

    def xfer_type2(self, msg):
        out_data = bytes('@', 'utf-8') + self._type2_seq.to_bytes(1, byteorder='big') + bytes('{}\n@@@@@@@@@@@@@'.format(msg), 'utf-8')
        self._type2_seq = (self._type2_seq + 1) if (self._type2_seq < 0xFF) else 0x67
        ret = self._dev.write(0x02, out_data[:16], 1000)
        in_data = bytes()
        while True:
            ret = bytes(self._dev.read(0x81, 0x10, 1000))
            if len(ret) == 0:
                break
            in_data += ret
        return in_data

    def _update_values(self):
        #logger.debug("iaqstick: update")
        try:
            self.xfer_type1('FLAGGET?')
            meas = self.xfer_type2('*TR')
            ppm = int.from_bytes(meas[2:4], byteorder='little')
            logger.debug('iaqstick: ppm: {}'.format(ppm))
            msg = {
                'iaq': ppm
            }
            self.publish(msg)
            #logger.debug('iaqstick: debug?: {}'.format(int.from_bytes(meas[4:6], byteorder='little')))
            #logger.debug('iaqstick: PWM: {}'.format(int.from_bytes(meas[6:7], byteorder='little')))
            #logger.debug('iaqstick: Rh: {}'.format(int.from_bytes(meas[7:8], byteorder='little')*0.01))
            #logger.debug('iaqstick: Rs: {}'.format(int.from_bytes(meas[8:12], byteorder='little')))
        except Exception as e:
            logger.error("iaqstick: update failed - {}".format(e))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    iaqstick = IAQStick('IAQStick')

    def signal_handler(signal_frame):
        def run(*args):
            print('Stopping IAQStick')
            iaqstick.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    iaqstick.start()
