import json
import signal
import serial
import sys
import numpy as np
from threading import Thread, Timer
from generic_sensor import GenericSensor


class IrlynxPeoplecounter(GenericSensor):

    raw_image_invert = False
    raw_image_rescale = True
    coordinates_normalize = 80
    sensor_type = 'peoplecounter_absolute'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self.load_config()
        self.persons = {}
        self.dimensions = []
        self.raw_image = b''
        self._stop_reading_sensor = False
        self._stop_report_timer = False
        self._stop_raw_timer = False
        self.raw_timer = None
        self.report_timer = None

    def load_config(self):
        self.serialport = self.load_config_value(self.name, 'serialport', default='/dev/ttyUSB0')
        self.baudrate = self.load_config_value(self.name, 'baudrate', default='115200')

    def publish_sensor_data(self):
        self.report_timer_restart(5, self.publish_sensor_data)
        msg = {
            "count": len(self.persons),
            "coords": self.persons
        }
        print(msg)
        self.publish(msg)

    def report_timer_restart(self, timeout, method):
        self.report_timer.cancel()
        if not self._stop_report_timer:
            self.report_timer = Timer(timeout, method)
            self.report_timer.start()

    def get_raw_image(self, ser):
        print("Get RAW image.")
        ser.write(b'\x52\x00')

    def raw_timer_timeout(self, **kwargs):
        self.get_raw_image(kwargs['ser'])
        if not self._stop_raw_timer:
            self.raw_timer = Timer(3, self.raw_timer_timeout, kwargs=kwargs)
            self.raw_timer.start()

    def start_timers(self, ser):
        self.raw_timer = Timer(3, self.raw_timer_timeout, kwargs={'ser': ser})
        self.raw_timer.start()
        self.report_timer = Timer(5, self.publish_sensor_data)
        self.report_timer.start()

    def start(self):
        self.mqtt_start()
        self.read_sensor()

    def stop(self):
        self._stop_reading_sensor = True
        self._stop_raw_timer = True
        if self.raw_timer:
            self.raw_timer.cancel()
        self._stop_report_timer = True
        if self.report_timer:
            self.report_timer.cancel()
        self.mqtt_stop()

    def read_sensor(self):
        synchronized = False
        try:
            with serial.Serial(self.serialport, self.baudrate, timeout=1) as ser:
                self.start_timers(ser)
                ser.write(b'\x10\x00')
                ser.write(b'\x1a\x00')
                while not self._stop_reading_sensor:
                    byte = ser.read(1)
                    if byte == b'':
                        print('timeout')
                        continue
                    if not synchronized:
                        if byte in [b'\x10', b'\x1a', b'\x52', b'\x53', b'\x33']:
                            synchronized = True
                    if synchronized:
                        length = int.from_bytes(ser.read(1), byteorder='little')
                        para = ser.read(length)
                        if byte == b'\x10':	# Serial number
                            self.serial_number = str(para)
                            print('Serial: {}'.format(self.serial_number))
                        elif byte == b'\x1a':	# Firmware version
                            self.firmware_version = str(para)
                            print('FW-Version: {}'.format(self.firmware_version))
                        elif byte == b'\x52':	# Wait for raw image
                            if para == b'OK':
                                print("RAW image incoming...")
                        elif byte in [b'\x53']:	# Read raw image
                            linenumber = para[0]
#                            print("RAW #{}".format(linenumber))
                            data = para[1:]
                            if linenumber == 0:
                                self.raw_image = data
                            else:
                                self.raw_image = self.raw_image + data
                            if linenumber == 39:
                                try:
                                    img = np.frombuffer(self.raw_image, dtype=np.uint8).reshape((40, 40))
                                    img = np.rot90(img)
                                    img = np.flipud(img)
                                    msg = {
                                        "count": len(self.persons),
                                        "coords": self.persons,
                                        "rawData": img.tolist()
                                    }
                                    self.publish(msg)
                                except Exception as e:
                                    print(e)
                                self.raw_timer.cancel()
                                if not self._stop_raw_timer:
                                    self.raw_timer = Timer(3, self.raw_timer_timeout, kwargs={'ser': ser})
                                    self.raw_timer.start()
                        elif byte == b'\x33':
                            # Single person is reported
                            id = int(para[0])
                            x = min(float(para[2]) / self.coordinates_normalize, 1.0)
                            y = min(float(para[3]) / self.coordinates_normalize, 1.0)
                            position = {'x': x, 'y': y}
                            if position not in self.persons.values():
                                self.persons[id] = position
                                self.publish_sensor_data()
                            else:
                                print("Ignoring double counting")
                        elif byte in [b'\34', b'\x35', b'\x36']:
                            id = int(para[0])
#                            print("Leave ", id)
                            if id in self.persons:
                                self.persons.pop(id)
                                self.publish_sensor_data()
                        elif byte == b'\x22':
                            presence = int(para[0])
                            if presence == 0:
                                self.persons.clear()
                                self.publish_sensor_data()
#                                print("Presence ", presence)
                        elif byte == b'\x23':
                            n = int(para[0])
                            if n == 0:
                                self.persons.clear()
                                self.publish_sensor_data()
#                            print("Number of people ", n)
                    else:
                        synchronized = False
                        print("sync-problem")
        except Exception as e:
            print("Error!")
            print(e)
            self.stop()


if __name__ == "__main__":
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    irlynx = IrlynxPeoplecounter(sys.argv[1])

    def signal_handler(signal, frame):
        def run(*args):
            print('Stopping sensor...')
            irlynx.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)

    Thread(target=irlynx.start).start()
