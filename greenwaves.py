from PIL import Image
import base64
import io
import json
import signal
import struct
import sys
from threading import Thread, Timer
import time
from generic_sensor import GenericSensor
from bluepy.btle import Scanner, DefaultDelegate, Peripheral, UUID, BTLEException, ADDR_TYPE_RANDOM

class GreenWavesPeopleCounter(GenericSensor):

    sensor_type = 'peoplecounter_absolute'

    def __init__(self, name):
        GenericSensor.__init__(self, name)
        self._stop_recv_timeout_timer = False
        self._recv_timeout_timer = None
        self._recv_timeout = None
        self.persons = []
        self.stop_sensor = False
        self.load_config()

    def load_config(self):
        changed = False
        if self.config.has_option(self.name, 'recv_timeout'):
            self._recv_timeout = int(self.config.get(self.name, 'recv_timeout').value)
        else:
            self.config.set(self.name, 'recv_timeout', '20')
            changed = True
        if changed:
            self.save_config()
            self.load_config()

    def publish_sensor_data(self):
        self._recv_timeout_timer.cancel()
        self._recv_timeout_timer = Timer(self._recv_timeout, quit)
        self._recv_timeout_timer.start()
        msg = {
            "count": len(self.persons),
            "coords": self.persons
        }
        print(msg)
        self.publish(msg)


    class NotificationDelegate(DefaultDelegate):
        transferInProgress = False

        def __init__(self, parent):
            DefaultDelegate.__init__(self)
            self.payload = b''
            self.imagePayload = b''
            self.imagePayloadLength = 0
            self.currentPayloadLength = 0
            self.parent = parent

        def handleNotification(self, cHandle, data):
#            print('-'*20)
#            print(f'Data: {data}')
            print(f"{self.imagePayloadLength}: {len(self.imagePayload)}: {self.currentPayloadLength}: {len(data)}")
            self.payload += data

            begin = self.payload.find(b'\x02')
            end = self.payload.find(b';\x03')
            if self.currentPayloadLength == 0 and begin != -1 and end != -1 and begin < end:
                if (end - begin) < 30:
#                    print("="*30)
#                    print(begin, end+2)
#                    print(self.payload[begin:end])
#                    print("="*30)
                    d = self.payload[begin:end].split(b';')
                    count = int(d[0].strip(b'\x02'))
                    coords = {}
                    self.parent.persons = [{'x': int(a[0])/80, 'y': int(a[1])/80} for a in [x.split(b'x') for x in d[1:count+1]]]
                    self.parent.publish_sensor_data();
                    self.payload = self.payload[end:]

            if self.currentPayloadLength > 0:
                if self.imagePayloadLength > 0:
                    self.imagePayload += self.payload
                    self.currentPayloadLength -= len(self.payload)
                    self.payload = b''

#                    print(f"IMG: {self.imagePayloadLength}: {len(self.imagePayload)}: {self.currentPayloadLength}: {len(self.payload)}")
                else:
                    self.currentPayloadLength -= len(self.payload)
                    self.payload = b''

                return

            # Search for 0x05 first, as 0x04 could be part of the following binary data
            begin = self.payload.find(b'\x05')
            if begin != -1 and len(self.payload)-begin > 1:
                self.currentPayloadLength = self.payload[begin+1]
                length = min(self.currentPayloadLength, len(self.payload[begin+2:]))
                if self.imagePayloadLength > 0:
                    self.imagePayload += self.payload[begin+2:begin+2+length]
                self.currentPayloadLength -= length
                self.payload = self.payload[begin+2+length:]

            begin = self.payload.find(b'\x04')
            if self.imagePayloadLength == 0 and begin != -1 and len(self.payload)-begin > 2:
                self.currentPayloadLength = self.payload[begin+1]
                self.imagePayloadLength = struct.unpack('<H', self.payload[begin+2:begin+1+3])[0]
                print(f'New image incoming: CurPayLen: {self.currentPayloadLength}, ImgPayLen: {self.imagePayloadLength}')
                self.payload = self.payload[begin+1+self.currentPayloadLength:]
                self.currentPayloadLength = 0


            if self.imagePayloadLength > 0 and len(self.imagePayload) == self.imagePayloadLength:
                print("Image received!\n")
                with open('image.img', 'wb') as file:
                    file.write(self.imagePayload)
                image = Image.open(io.BytesIO(self.imagePayload))
                print(image)
                with io.BytesIO() as output:
                    image.save(output, 'PNG')
                    img = output.getvalue();
                msg = { "img": str(base64.b64encode(img), 'ascii'),
                        "raw": str(base64.b64encode(self.imagePayload[2:]), 'ascii'),
                }
                self.imagePayload = b''
                self.imagePayloadLength = 0
                self.parent.publish(msg)



        def handleDiscovery(self, dev, isNewDev, isNewData):
            if isNewDev:
              pass

            elif isNewData:
                print("Received new data from", dev.addr)




    def start(self):
        self.mqtt_start()
        self._recv_timeout_timer = Timer(self._recv_timeout, quit)

        scanner = Scanner().withDelegate(GreenWavesPeopleCounter.NotificationDelegate(self))
        devices = scanner.scan(10.0)

        for dev in devices:
            if (dev.getValue(9) == "GreenWaves-GAPOC"):
                print("Discovered device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
                p = Peripheral(dev.addr)
                p.withDelegate(GreenWavesPeopleCounter.NotificationDelegate(self))
                c = p.getCharacteristics(uuid="2456e1b9-26e2-8f83-e744-f34f01e9d703")
                print("Characteristics:\n")
                print(c)
                if len(c) > 0:
                    c = c[0]
                    ConfigHndl = c.valHandle + 1
                    print(p.writeCharacteristic(ConfigHndl , (1).to_bytes(2, byteorder='little'), withResponse=True))
                    print("Request")
                    print(c.write((90).to_bytes(1, byteorder='little')))
                    print(p)
                    print(c)
                    print("---")
                    while(not self.stop_sensor):
                        time.sleep(0.1)
                        c.write((90).to_bytes(1, byteorder='little'), withResponse=True)

    def stop(self):
        self.stop_sensor = True
        self._recv_timeout_timer.cancel()
        try:
            pass
        except Exception as e:
            print(e)
        self.mqtt_stop()
        print("Stopped. Ignore the previous error.")


if __name__ == "__main__":
    if (len(sys.argv) <= 1):
        print('Usage: {} <name>'.format(sys.argv[0]))
        quit()

    pc = GreenWavesPeopleCounter(sys.argv[1])

    def signal_handler(signal, frame):
        def run(*args):
            print('Stopping sensor...')
            pc.stop()
        Thread(target=run).start()

    signal.signal(signal.SIGINT, signal_handler)


    pc.start()
